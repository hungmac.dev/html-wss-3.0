$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'

$appcmd = 'C:\Windows\system32\inetsrv\appcmd'
try {
    Import-Module WebAdministration
}
catch { }

function RunWebPack() {
    if (Test-Path ./already-build) {
        echo "Build already done"
    }
    else {
        Set-Content -Path ./already-build -Force "build done"
        &(Get-Command build-bm4).Path
    }
}

Function wpnpm() {
    echo "installing npm package wpbuild"
    npm i -g ../build-bm4-1.0.4.tgz
}

Function installWpBuild($version) {
    try {
        $version = $version.Split('\\')[-1]
        $modPath = [System.IO.Path]::GetDirectoryName((Get-Command build-bm4).Path)
        
        $pkgj = "$modPath\node_modules\build-bm4\bin\$version"
        # $pkgj = "$modPath\node_modules\build-bm4\bin\064511fbac726a9cd41bfa54c2567675f9f21dd2"
        echo "checking to install $pkgj"
        if (Test-Path $pkgj) {
            echo "already installed lastest version"
        }
        else {
            wpnpm
        }
    }
    catch {
        echo "install new builder"
        wpnpm
    }
}

function preLoadLink () {
    foreach($line in Get-Content .\WebsosanhCheckLink\Links.txt) {
        Write-Host $line
        $rsa = curl.exe -X GET -I -s $line
        #Write-Host 'test1'
        if ($rsa) {
                $rs = $rsa[0]
                if ($rs -match "200") {
                    $loop = 0
                    $rso = $rsa -join "`n"
                    Write-Host "load link: $line => OK"
                    #Write-Host $rso
                    #[Environment]::Exit(0)
                } 
                else {
                    Write-Host $rs
                }
            }
            else {
                Write-Host "Status response : $($rsa[0])" -BackgroundColor "Red"
                throw "exceed counter wait server ready!"
            }
    }
    [Environment]::Exit(0)
}


function logoffall() {
    $sessions = quser 
    Write-Host "Found $(@($sessions).Length) user login(s) on computer."
    foreach ($ses in $sessions) {
        $it = $ses.ToString().Trim() -split ' +'
        $id = $it.Get(2)
        if ($id -eq "Disc") {
            $id = $it.Get(1)
        }
        if (!($id -eq "ID")) {
            try {
                Write-Host "Logging off session id [$($id)]..."
                logoff $id
            }
            catch { }
        }
    }
}


Function RunProd($folder) {
    pushd $folder
    try {
        dotnet run -c Release --launch-profile Production
    }
    finally {
        popd
    }
}
Function RunNginx() {
    #Add-DnsServerResourceRecord -ZoneName "websosanh.vn" -A -Name "local.web-dt.vn" -IPv4Address "127.0.0.1"
    pushd .\nginx
    nginx
    popd
}

function BmInstallService($ServiceName, $source, $destination) {
    #    $ServiceName = "wss.financial"

    Write-Output "Stoping service ..."
    $svc = Get-Service -Name $ServiceName
    Stop-Service $svc
    $count = 0
    while ($svc.Status -ne "Stopped") {
        Start-Sleep -s 1
        $count = $count + 1
        if ($count -gt 60) {
            throw "Cannot stop service $ServiceName"
        }
    }
    Write-Output "Copy binaries file ..."

    Remove-Item $destination -Force -Recurse
    New-Item -ItemType directory -Path $destination
    Get-ChildItem $source | Copy-Item -Destination $destination -Recurse -Force

    #Copy-Item  -Force -Recurse -Path  WSS.Financial\bin\Debug\* -Destination C:\tungbt\financial
    #&"Copy-Item  -Force -Recurse -Path $($source)\* -Destination $($destination)"
    Write-Output "Starting service ..."
    Start-Service $svc
    $count = 0
    while ($svc.Status -ne "Running") {
        Start-Sleep -s 1
        $count = $count + 1
        if ($count -gt 60) {
            throw "Cannot start service $ServiceName"
        }
    }
}


Function WaitServerOnlineV2 ($linkCheck, $domain) {
    $obj = @{
        "link"     = "$($linkCheck)";
        "hostname" = $domain;
    }
    $src = New-Object -TypeName PSObject -Property $obj
    Write-Host  "checking $($src.link) on $($src.hostname)"
    $count = 0;
    $loop = 1
    $progressPreference = 'silentlyContinue'    # Subsequent calls do not display UI.
    try {
        #$headers = @{}
        #$headers.Add("Accept", "*")
        #$headers.Add("Host", $hostname )
        #$headers.Add("Content-Type", "application/json")
        #$headers.Add("x-sap-logontoken", $Token)
        while ($loop) {
            $count = $count + 1;
            Write-Host "checking link:$($src.link) $($x.StatusCode)"
            #$rsa = curl.exe -X GET -I -s $src.link -H "HOST:$($src.hostname)"
            $rsa = curl.exe -X GET -I -s $src.link 
            if ($rsa) {
                $rs = $rsa[0]
                if ($rs -match "HTTP/1.1 200 OK") {
                    $loop = 0
                    $rso = $rsa -join "`n"
                    Write-Host "Status of check health page:"
                    Write-Host $rso
                    [Environment]::Exit(0)
                }
            }
            else {
                Write-Host "Status response : $($rsa[0])" -BackgroundColor "Red"
                Start-Sleep -Seconds 2
            }
            if ($count -gt 60) {
                throw "exceed counter wait server ready!"
            }
        }
    }
    catch {
        [Environment]::Exit(1)
    }
    $progressPreference = 'Continue'
}

Function WaitServerOnline($ip, $domain, $health, $port) {
	
    $obj = @{
        "link"     = "http://$($ip):$($port)$($health)";
        "hostname" = $domain;
    }


    $src = New-Object -TypeName PSObject -Property $obj
    Write-Host  "checking $($src.link) on $($src.hostname)"
    $count = 0;
    $loop = 1
    $progressPreference = 'silentlyContinue'    # Subsequent calls do not display UI.
    try {
        #$headers = @{}
        #$headers.Add("Accept", "*")
        #$headers.Add("Host", $hostname )
        #$headers.Add("Content-Type", "application/json")
        #$headers.Add("x-sap-logontoken", $Token)
        while ($loop) {
            $count = $count + 1;
            Write-Host "checking link:$($src.link) $($x.StatusCode)"
            $rsa = curl.exe -X GET -I -s $src.link -H "HOST:$($src.hostname)"
            if ($rsa) {
                $rs = $rsa[0]
                if ($rs -match "HTTP/1.1 200 OK") {
                    $loop = 0
                    $rso = $rsa -join "`n"
                    Write-Host "Status of check health page:"
                    Write-Host $rso
                    [Environment]::Exit(0)
                }
            }
            else {
                Write-Host "Status response : $($rsa[0])" -BackgroundColor "Red"
                Start-Sleep -Seconds 2
            }

            if ($count -gt 60) {
                throw "exceed counter wait server ready!"
            }
        }
    }
    catch {
        [Environment]::Exit(1)
    }
    $progressPreference = 'Continue'
}
#try { $response = Invoke-WebRequest http://localhost/foo } catch {
#      $_.Exception.Response.StatusCode.Value__}
#$para='172.22.1.163'
#$cmd="http://$para/api/common/checkapi"
#waitServerOnline $cmd "webapi2.websosanh.vn"


#echo $src
Function AutoCreateWebSite($webSiteName, $webRunFolder) {
    Push-Location IIS:\
    try {
        $sites = Get-ChildItem sites
        $found = $sites.Where( { $_.Name -eq $webSiteName })
        if ($found.Length -gt 0) {
            Write-Output "found site"
            Write-Output $found[0]
        }
        else {
            Write-Output "site not found, creating ..."
            CreateWebsite $webSiteName $webSiteName
        }
        if (!(Test-Path $webRunFolder)) {
            mkdir $webRunFolder
        }
    }
    catch {
        Write-Output $_
    }
    finally {
        Pop-Location
    }
    
    

    
    #$site | foreach
    #cd "amp.dantri.com.vn" "amp.dantri.com.vn"
}
#AutoCreateWebSite "qwe" "c:\web\xxx"
Function DeployAll($webSiteName, $webPublishFolder, $webRunFolder) {
    logoffall
    try {
        get-command curl.exe
    }
    catch {
        choco install curl
    }

    AutoCreateWebSite $webSiteName $webRunFolder

    C:\Windows\system32\inetsrv\appcmd stop site $webSiteName

    C:\Windows\System32\inetsrv\appcmd stop apppool /apppool.name:$webSiteName

    do { Start-Sleep -s 1; $siteInfo = C:\Windows\System32\inetsrv\appcmd.exe list site $webSiteName } until ($siteInfo -match "state:Stopped")
    do { Start-Sleep -s 1; $siteInfo = C:\Windows\System32\inetsrv\appcmd.exe list apppool $webSiteName } until ($siteInfo -match "state:Stopped")
    Remove-Item $webRunFolder -Force -Recurse
    New-Item -ItemType directory -Path $webRunFolder
    Get-ChildItem $webPublishFolder | Copy-Item -Destination $webRunFolder -Recurse -Force

    #Copy-Item -Verbose -force -Recurse -Path $webPublishFolder -Destination $webRunFolder
    C:\Windows\System32\inetsrv\appcmd start apppool /apppool.name:$webSiteName

    C:\Windows\system32\inetsrv\appcmd start site $webSiteName

    if (Test-Path Env:LINK_TEST) {
        WaitServerOnlineV2 "$Env:LINK_TEST" "$Env:WEBDOMAIN"
	}

    #if (Test-Path Env:WEBIP) {
    #    WaitServerOnlineV2 "$Env:WEBIP" "$Env:WEBDOMAIN" "$Env:HEALTH" "$Env:PORT"
    #}
}

Function BuildWebAll ($projectFolder, $projectFile, $configurationBuider, $outputFolder) {
    echo "Move to project folder"
    cd $projectFolder
    echo "Restore tool"
    dotnet restore $projectFile
    echo "Build code"
    dotnet build $projectFile
    echo "Deploy code"
    dotnet publish $projectFile -c  $configurationBuider -o $outputFolder
}


Function CheckLastExitCode {
    param ([int[]]$SuccessCodes = @(0), [scriptblock]$CleanupScript = $null)

    if ($SuccessCodes -notcontains $LastExitCode) {
        if ($CleanupScript) {
            "Executing cleanup script: $CleanupScript"
            &$CleanupScript
        }
        $msg = @"
EXE RETURNED EXIT CODE $LastExitCode
CALLSTACK:$(Get-PSCallStack | Out-String)
"@
        throw $msg
    }
}
function RemoveTrashItemFilter($filter) {
    $cfg = $ENV:ASPNETCORE_ENVIRONMENT
    Get-ChildItem ".\bin\" -Ver -Recurse -Filter "$filter" | Remove-Item -Force
}
function RemoveTrashItem() {
    echo "Removing unused configuration files..."
    RemoveTrashItemFilter "app.*.config"
    RemoveTrashItemFilter "web.*.config"
    RemoveTrashItemFilter "appsettings.*.json"
    RemoveTrashItemFilter "log4net.*.config"
    RemoveTrashItemFilter "SharedBundle.json"
    RemoveTrashItemFilter "SharedSettings.json"
}
$env:Path = "${ENV:APPDATA}\npm;${env:Path}"
try {
    npm config set python C:\Python27\python.exe
    npm config set msvs_version 2017 --global
}
catch { }

Function Build($loc) {
    $env:deploy = "1"
    # Set-Content C:\Windows\System32\config\SystemProfile\.npmrc "prefix=C:\npm-global"
  
    
    # npm config set prefix=C:\npm-global
    # npm i stackframe
    try {
        #remove old sources
        nuget sources remove -name wss
        
    }
    catch { }
    try {
        nuget sources add -name wss -source "http://lib.websosanh.org/httpAuth/app/nuget/v1/FeedService.svc" -username nuget -password 123456a@ -StorePasswordInClearText
    }
    catch { }
    
    nuget sources  

    #taskkill /f /im dotnet.exe
    $cfg = $ENV:ASPNETCORE_ENVIRONMENT
    echo "Enviroment for build $cfg"
    $pkgPath = (pwd).Path
    #$ld="cmd /c mklink /j $pkgPath\packages c:\packages"
    #echo $ld
    echo "building directory $loc"

    #iex "& $ld"
    #nuget config -set repositoryPath=c:\packages

    nuget restore -PackagesDirectory .\packages -configFile NuGet.Config Web.sln
    #nuget restore -configFile NuGet.Config -PackagesDirectory c:\packages
    #
    pushd $loc


    #Copy-Item gulpfile.js c:\nodebuild\* .
    #docker build -t webdocker .
    #docker run -rm -t webdocker
    try {
		
        #&"C:\Users\tbt\AppData\Roaming\npm\build-bm4.ps1"
        #dotnet restore
        dotnet build -c $cfg $($loc).csproj --packages c:\packages  -p:UseSharedCompilation=false -p:UseRazorBuildServer=false
        CheckLastExitCode

        #iex "& $cmd"
        dotnet publish -c $cfg --packages c:\packages -p:UseSharedCompilation=false -p:UseRazorBuildServer=false
		
        # this is to keep aol version of dantri android app
        if (Test-Path "bin\$($cfg)\net461\publish\wwwroot\bundle\static\mobile.min.css") {
            copy bin\$($cfg)\net461\publish\wwwroot\bundle\static\mobile.min.css bin\$($cfg)\net461\publish\wwwroot\bundle\static\dtm.min.css
        }

        RemoveTrashItem
        #taskkill /f /im dotnet.exe
    }
    catch {
        throw $_.Exception
    }
    finally {
        popd
        #rmdir .\packages\ -Force -Recurse

    }

    #Remove-Item -Path .\bin\Release -Force -Recurse -Include *.config,*.xml,*.pdb,*.json
}

Function installMod() {
    npm config set python C:\Python27\python.exe
    npm config set msvs_version 2017 --global
    #npm i -g fibers
    npm i -g node-sass
    #npm i -g windows-build-tools
    #npm i -g build-bm4-1.0.4.tgz --force
}

# Function CleanUpPackages() {
#   taskkill /f /im dotnet.exe
#   Remove-Item .\packages -Force -Recurse
# }
# mkdir betacomment.tintucdantri.com
# New-WebSite -Name betacomment.tintucdantri.com -Port 80 -HostHeader betacomment.tintucdantri.com -PhysicalPath "c:\web\betacomment.tintucdantri.com"
Function SetupNetCoreAppPool($poolName) {
    $applicationPools = Get-ChildItem IIS:\AppPools
    ForEach ($appPool in $applicationPools) {
        if ($appPool.name -eq $poolName) {
            $appPath = "IIS:\AppPools\" + $appPool.name
            $appPool.managedRuntimeVersion = ""
            Set-ItemProperty -Path $appPath -Name managedRuntimeVersion $appPool.managedRuntimeVersion
            break
        }
    }

}

Function CreateWebsite($website, $hostheader) {
    try {
        $cmd = "mkdir c:\web\$website"
        iex "& $cmd"
    }
    catch {
    }
    try {
        New-WebAppPool -Name $website
    }
    catch { }
    #New-WebBinding -Name $website -IPAddress "*" -Port 80 -HostHeader $website
    New-WebSite -Name $website -Port 80 -HostHeader $hostheader -PhysicalPath "c:\web\$website" -ApplicationPool $website -Force
    SetupNetCoreAppPool $website
    # setup another website binding
    #New-WebBinding -Name $website -IPAddress "*" -Port 80 -HostHeader $hostheader -Force

}


Function CreateAllWebSite() {
    CreateWebsite "betacaptcha.tintucdantri.com" "betacaptcha.tintucdantri.vn"
    CreateWebsite "betacomment.tintucdantri.com" "betacomment.tintucdantri.vn"
    CreateWebsite "betaduhoc.tintucdantri.com" "betaduhoc.tintucdantri.vn"
    CreateWebsite "betadulich.tintucdantri.com" "betadulich.tintucdantri.vn"
    CreateWebsite "betamobile.tintucdantri.com" "betamobile.tintucdantri.vn"
    CreateWebsite "betatuyensinh.tintucdantri.com" "betatuyensinh.tintucdantri.vn"
    CreateWebsite "betaweb.tintucdantri.com" "betaweb.tintucdantri.vn"
    CreateWebsite "webapi.tintucdantri.com" "webapi.tintucdantri.vn"
}

Function BuildAll() {
    Build Wss.Baomoi.Captcha
    Build Wss.Baomoi.Comment
    Build Wss.Baomoi.Duhoc
    Build Wss.Baomoi.Dulich
    Build Wss.Baomoi.Mobile
    Build Wss.Baomoi.Tuyensinh
    Build Wss.Baomoi.Web
    #Remove-Item -Path .\bin\Release -Force -Recurse -Include *.config,*.xml,*.pdb,*.json
}
function UpdateCDNVersionFolder($dir) {
    Get-ChildItem "$dir\wwwroot\bundle\*" -recurse | ForEach-Object { $_.LastWriteTime = Get-Date }
}
function UpdateCDNVersion() {
    UpdateCDNVersionFolder C:\web\betaweb.tintucdantri.com
    UpdateCDNVersionFolder C:\web\betamobile.tintucdantri.com
}

Function BuildWeb () {
    BuildWebAll "$Env:PROJECT_FOLDER_BUILD" "$Env:PROJECT_FILE_BUILD" "$Env:CONFIG_BUILD" "$Env:OUTPUT_BUILD"
}

Function BuildAMP () {
    BuildWebAll "$Env:PROJECT_FOLDER_BUILD" "$Env:PROJECT_FILE_BUILD" "$Env:CONFIG_BUILD" "$Env:OUTPUT_BUILD"
}

Function BuildSoSanhDanTri () {
    BuildWebAll "$Env:PROJECT_FOLDER_BUILD" "$Env:PROJECT_FILE_BUILD" "$Env:CONFIG_BUILD" "$Env:OUTPUT_BUILD"
}

Function DeployHtml () {
    DeployAll "$Env:WEBDOMAIN" "Source" "$Env:LOCATION_DEPLOY"
}

Function DeployWebsosanhWeb () {
    DeployAll "$Env:WEBDOMAIN" "Wss.Website/Output" "$Env:LOCATION_DEPLOY"
}

Function DeployWebSoSanhDanTri () {
    DeployAll "$Env:WEBDOMAIN" "PresentationLayer.SoSanhDanTri/SoSanhDanTri.Website/Output" "$Env:LOCATION_DEPLOY"
}


Function DeployWebsosanhAMP () {
    DeployAll "$Env:WEBDOMAIN" "Wss.Amp/Output" "$Env:LOCATION_DEPLOY"
}

#Import the web administration module
#Clear-Host

#Import the web administration module
#Import-Module WebAdministration -ErrorAction Stop


# $lastDeployTag=git describe --tags --abbrev=0
# echo $lastDeployTag

# $pm="git log $lastDeployTag..HEAD --oneline"
# echo $pm
# $msg=iex "& $pm" | foreach {"*  " + $_ }  | out-string
# echo $msg


Function gitapitag() {
    $msg = ""
    $GitAPIServer = "https://gitlab.com/api/v4"
    $TagId = "$($env:CI_PIPELINE_ID)"
    $ProjectId = "$($env:CI_PROJECT_ID)"
    $Token = "NkJK3ydAsPciFVKe32-h"
    $Url = "$GitAPIServer/projects/$ProjectId/repository/tags"
    Write-Output "Create tag for $TagId $Url"
    #$headers = @{
    #    "Private-Token" = "$Token"
    #}
    $TagPrefix = Get-Date -UFormat "%Y%m%d-%A"

    try {

        #$res=(Invoke-WebRequest -Method GET -Uri "$GitAPIServer/projects/$ProjectId/repository/tags" -ContentType "application/json" -Headers $headers  -UseBasicParsing).Content | ConvertFrom-Json
        $res = C:\ProgramData\chocolatey\bin\curl.exe -s -k "$GitAPIServer/projects/$ProjectId/repository/tags" -H "ContentType: application/json" -H "Private-Token: $Token" | ConvertFrom-Json
        
        if ($res) {
            #echo $res[0].commit.committed_date
            $bdq = @{since = $res[0].commit.committed_date } | ConvertTo-Json -Compress
            #$res2=(Invoke-WebRequest -Method GET -Uri "$GitAPIServer/projects/$ProjectId/repository/commits?since=$($res[0].commit.committed_date)" -ContentType "application/json" -Headers $headers -UseBasicParsing).Content | ConvertFrom-Json
            $res2 = C:\ProgramData\chocolatey\bin\curl.exe -s -k  "$GitAPIServer/projects/$ProjectId/repository/commits?since=$($res[0].commit.committed_date)" -H "ContentType: application/json" -H "Private-Token: $Token" | ConvertFrom-Json
            #echo ($res2 | ConvertTo-Json)
            $outItems = @()
            ForEach ($item in $res2) {
                $itemExtract = $item.title.Split("*") | ForEach-Object ( { return $_.Trim() }) | Where-Object ( { return !($_ -eq "") }) | ForEach-Object ( { return "$($item.short_id) - * $_" })
                
                #$mstr = @($item.short_id, $item.title -join " - ")
                
                $outItems += $itemExtract # += $("* " + $mstr)
            }
            #echo ($outItems | ConvertTo-Json)
            $msg = $($outItems -join " `n")
        }

    }
    catch { 
        Write-Output ($_ | ConvertTo-Json)
    }
    Write-Output $msg
    # can them CI_PIPELINE_ID vi neu muon deploy nhieu lan trong ngay
    #$data="{ `"ref`":`"master`",`"tag_name`":`"$TagPrefix-$($env:CI_PIPELINE_ID)`",`"release_description`":`"Release version: [$($env:CI_PIPELINE_ID)]($($env:CI_PROJECT_URL)/pipelines/$($env:CI_PIPELINE_ID))`" }"
    #echo $data
    #$data2= @{ref="master";tag_name="$TagPrefix-$($env:CI_PIPELINE_ID)";release_description=[System.Web.HttpUtility]::UrlEncode("Release version: $($env:CI_COMMIT_REF_NAME) [$($env:CI_PIPELINE_ID)]($($env:CI_PROJECT_URL)/pipelines/$($env:CI_PIPELINE_ID)) `n $msg")} 
    #Invoke-WebRequest -Method Post -Uri "$Url" -ContentType "application/json" -Headers $headers -Body $data2 -UseBasicParsing
    #echo $data2
    #git tag -a "$TagPrefix-$($env:CI_PIPELINE_ID)" -m "Release version: $($env:CI_COMMIT_REF_NAME) [$($env:CI_PIPELINE_ID)]($($env:CI_PROJECT_URL)/pipelines/$($env:CI_PIPELINE_ID)) `n $msg"
    $gitlab_tag_name = "$TagPrefix-$($env:CI_PIPELINE_ID)"
    $gitlab_release_description = [System.Web.HttpUtility]::UrlEncode("Release version: $($env:CI_COMMIT_REF_NAME) [$($env:CI_PIPELINE_ID)]($($env:CI_PROJECT_URL)/pipelines/$($env:CI_PIPELINE_ID)) `n $msg")
	
    $cmdtag = "C:\ProgramData\chocolatey\bin\curl.exe -s -k -X POST `"$($Url)?tag_name=$($gitlab_tag_name)&ref=master&release_description=$($gitlab_release_description)`" -H `"ContentType: application/json`" -H `"Private-Token: $Token`""
    Write-Output $cmdtag
    Invoke-Expression "& $cmdtag"
    #Invoke-WebRequest -Method Post -Uri "$Url" -ContentType "application/json" -Headers $headers -Body $data2 -UseBasicParsing


}

#$env:CI_PIPELINE_ID = "62135072"
#$env:CI_PROJECT_ID = "6353458"
#gitapitag
#     $TagId="$($env:CI_PIPELINE_ID)"
#     $ProjectId="$($env:CI_PROJECT_ID)"
# $Token="NkJK3ydAsPciFVKe32-h"
#     $Url="$GitAPIServer/projects/$ProjectId/repository/tags"
#     echo "Create tag for $TagId $Url"
#     $headers = @{
#         "Private-Token" = "$Token"
#     }
#     $TagPrefix=Get-Date -UFormat "%Y%m%d-%A"
#     $res=curl.exe -k "$GitAPIServer/projects/$ProjectId/repository/tags" -H "ContentType: application/json" -H "Private-Token: $Token" | ConvertFrom-Json
#     #$res=(Invoke-WebRequest -Method GET -Uri "$GitAPIServer/projects/$ProjectId/repository/tags" -ContentType "application/json" -Headers $headers  -UseBasicParsing).Content | ConvertFrom-Json
#     echo $res

#$a=[System.Web.HttpUtility]::UrlEncode("some-tag-name");
#echo $a
#curl.exe -k -X POST "https://gitlab.com/api/v4/projects/6353458/repository/tags?tag_name=41411533&ref=master&release_description=some%20release" -H "ContentType: application/json" -H "Private-Token: NkJK3ydAsPciFVKe32-h" -d '{"tag_name":"41411533","ref":"master","release_description":"x"}'
function makeIISRule($farmName, $suffix, $hostName, $port) {
    $ruleName = "$($farmName)$($suffix)-rule"
    $foundRules = Get-WebConfigurationProperty -Name "."  -Filter "/system.webServer/rewrite/globalRules/rule[@name='$($ruleName)']"
    if ( $foundRules  ) {
   
    }
    else {
        $obj = @{
     
            name           = $ruleName
        
            patternSyntax  = "ECMAScript" 
            stopProcessing = "true"
            match          = @{
                url = ".*"
          
            }
            action         = @{ 
                type = "Rewrite" 
                url  = "http://$($farmName)/{R:0}" 
            }
    
      
      
       
    
        }
        $cond1 = @{
            input   = "{HTTP_HOST}" 
            pattern = "^$($hostName)$"
        } 
        $cond2 = @{
            input   = "{SERVER_PORT}" 
            pattern = "^$($port)$"
      
        }
        Add-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -Name "." -Filter "/system.webServer/rewrite/globalRules" -Value $obj
        Add-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -Name "." -Filter "/system.webServer/rewrite/globalRules/rule[@name='$($ruleName)']/conditions" -Value $cond1
        Add-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -Name "." -Filter "/system.webServer/rewrite/globalRules/rule[@name='$($ruleName)']/conditions" -Value $cond2
    }
}
function AddHostEntry {
    Param(
        
        [string[]] $Hosts
       
    )
    
    $HostFile = 'C:\Windows\System32\drivers\etc\hosts'
 
    # Create a backup copy of the Hosts file
    $dateFormat = (Get-Date).ToString('dd-MM-yyyy hh-mm-ss')
    $FileCopy = $HostFile + '.' + $dateFormat + '.copy'
    Copy-Item $HostFile -Destination $FileCopy
 
    #Hosts to Add
    #$Hosts = @("intranet.Crescent.com", "Intranet", "mysite.crescent.com")
 
    # Get the contents of the Hosts file
    $File = Get-Content $HostFile
 
    # write the Entries to hosts file, if it doesn't exist.
    foreach ($HostFileEntry in $Hosts) {
        #Write-Host "Checking existing HOST file entries for $HostFileEntry..."
     
        #Set a Flag
        $EntryExists = $false
     
        if ($File -match "127.0.0.1 `t $HostFileEntry") {
            Write-Host "Host File Entry for $HostFileEntry is already exists."
            $EntryExists = $true
        }
        #Add Entry to Host File
        if (!$EntryExists) {
            Write-host "Adding Host File Entry for $HostFileEntry"
            Add-content -path $HostFile -value "127.0.0.1 `t $HostFileEntry"
        }
    }



}

#$vara = get-webconfigurationproperty '/system.webServer/rewrite/rules[@name="m-mobile"]'
function makeIISWebFarmRules($farmName, $hostName) {
    makeIISRule $farmName "" $hostName 80
    makeIISRule $farmName "-https" $hostName 443
}
function makeIISWebFarm {
    Param(
        [string] $FarmName,
        [string] $HostName,
        [Array[]] $Servers
    )
    makeIISWebFarmRules $farmName $hostName
    $foundRules = Get-WebConfigurationProperty -Name "."  -Filter "/webFarms/webFarm[@name='$($FarmName)']"
    if ($foundfoundRules) {

    }
    else {
        $obj = @{
            name    = "$($FarmName)" 
            enabled = "true"
        }
       
        Add-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -Name "." -Filter "/webFarms" -Value $obj
        $enable = "true"

        ForEach ($srv in $Servers) {
            $arp = @{ }
            if ($srv.port) {
                $arp.httpPort = $srv.port
            }
            if ($srv.host) {
                $arp.hostName = $srv.host
            }
            $server = @{
                address                   = "$($srv.ip)" 
                enabled                   = $enable
                applicationRequestRouting = $arp
            }
            $enable = "false"
            Add-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -Name "." -Filter "/webFarms/webFarm[@name='$($FarmName)']" -Value $server
        }
       
    }
  
}

function createWebFarm() {
    AddHostEntry @("dantri.com.vn", "cdnweb.dantri.com.vn", "web", "web-cdn")
    $srvs = @(
        @{ip = "192.168.0.203"; host = "dantri.com.vn" },
        @{ip = "192.168.0.204"; host = "dantri.com.vn" },
        @{ip = "192.168.0.205"; host = "dantri.com.vn" },
        @{ip = "192.168.0.192"; host = "dantri.com.vn" }, 
        @{ip = "192.168.0.168"; host = "dantri.com.vn" },
        @{ip = "192.168.0.169"; host = "dantri.com.vn" },
        @{ip = "127.0.0.1"; port = 51000 })
    makeIISWebFarm "web" "dantri.com.vn" $srvs
    makeIISWebFarm "web-cdn" "cdnweb.dantri.com.vn" $srvs
}
function createMobileFarm() {
    AddHostEntry @("m.dantri.com.vn", "cdnmobile.dantri.com.vn", "mobile", "mobile-cdn")
    $srvs = @(
        @{ip = "192.168.0.203"; host = "m.dantri.com.vn" },
        @{ip = "192.168.0.204"; host = "m.dantri.com.vn" },
        @{ip = "192.168.0.205"; host = "m.dantri.com.vn" },
        @{ip = "192.168.0.192"; host = "m.dantri.com.vn" }, 
        @{ip = "localhost"; port = 51002 })
    makeIISWebFarm "mobile" "m.dantri.com.vn" $srvs
    makeIISWebFarm "mobile-cdn" "cdnmobile.dantri.com.vn" $srvs
}

function createWebApiFarm() {
    AddHostEntry @("webapi.dantri.com.vn", "webapi")
    $srvs = @(
        
        @{ip = "localhost"; port = 54141 })
    makeIISWebFarm "webapi" "webapi.dantri.com.vn" $srvs
    
}
function createWebApiTestFarm() {
    AddHostEntry @("webapi.dantri.test", "webapi-test")
    $srvs = @(
        
        @{ip = "localhost"; port = 54141 })
    makeIISWebFarm "webapi-test" "webapi.dantri.test" $srvs
    
}

function createWebFarmTest() {
    AddHostEntry @("dantri.test", "cdnweb.dantri.test", "web-test", "web-cdn-test")
    $srvs = @(
        
        
        
        
        @{ip = "127.0.0.1"; port = 51000 })
    makeIISWebFarm "web-test" "dantri.test" $srvs
    makeIISWebFarm "web-cdn-test" "cdnweb.dantri.test" $srvs
}
function createMobileTest() {
    AddHostEntry @("m.dantri.test", "cdnmobile.dantri.test", "m-test", "m-cdn-test")
    $srvs = @(
        @{ip = "192.168.9.20"; host = "m.dantri.test" },
        @{ip = "192.168.9.139"; host = "m.dantri.test" },
        @{ip = "127.0.0.1"; port = 51000 })
    makeIISWebFarm "m-test" "m.dantri.test" $srvs
    makeIISWebFarm "m-cdn-test" "cdnmobile.dantri.test" $srvs
}

function createAmp() {
    AddHostEntry @("amp.dantri.com.vn", "cdnamp.dantri.com.vn", "amp-vn", "amp-cdn-vn")
    $srvs = @(
        @{ip = "192.168.0.203"; host = "amp.dantri.com.vn" },
        @{ip = "192.168.0.204"; host = "amp.dantri.com.vn" },
        @{ip = "192.168.0.205"; host = "amp.dantri.com.vn" },
        @{ip = "192.168.0.192"; host = "amp.dantri.com.vn" }, 
        @{ip = "127.0.0.1"; port = 51002 }
    )
    makeIISWebFarm "amp-vn" "amp.dantri.com.vn" $srvs
    makeIISWebFarm "amp-cdn-vn" "cdnamp.dantri.com.vn" $srvs
}


function createDuhoc() {
    AddHostEntry @("duhoc.dantri.com.vn", "cdnduhoc.dantri.com.vn", "duhoc-vn", "duhoc-cdn-vn")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51004 }
    )
    makeIISWebFarm "duhoc-vn" "duhoc.dantri.com.vn" $srvs
    makeIISWebFarm "duhoc-cdn-vn" "cdnduhoc.dantri.com.vn" $srvs
}
function createTuyenSinhTest() {
    AddHostEntry @("tuyensinh.dantri.test", "cdntuyensinh.dantri.test", "tuyensinh-test", "tuyensinh-cdn-test")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51001 }
    )
    makeIISWebFarm "tuyensinh-test" "tuyensinh.dantri.test" $srvs
    makeIISWebFarm "tuyensinh-cdn-test" "cdntuyensinh.dantri.test" $srvs
}
function createTuyenSinh() {
    AddHostEntry @("tuyensinh.dantri.com.vn", "cdntuyensinh.dantri.com.vn", "tuyensinh-vn", "tuyensinh-cdn-vn")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51001 }
    )
    makeIISWebFarm "tuyensinh-vn" "tuyensinh.dantri.com.vn" $srvs
    makeIISWebFarm "tuyensinh-cdn-vn" "cdntuyensinh.dantri.com.vn" $srvs
}
function createCaptcha() {
    AddHostEntry @("captcha.dantri.com.vn", "cdncaptcha.dantri.com.vn", "captcha-vn", "captcha-cdn-vn")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51001 },
        @{ip = "192.168.0.192"; host = "captcha.dantri.com.vn" }
    )
    makeIISWebFarm "captcha-vn" "captcha.dantri.com.vn" $srvs
    makeIISWebFarm "captcha-cdn-vn" "cdncaptcha.dantri.com.vn" $srvs
}

function createDuLichTest() {
    AddHostEntry @("dulich.dantri.test", "cdndulich.dantri.test", "dulich-test", "dulich-cdn-test")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51003 }
    )
    makeIISWebFarm "dulich-test" "dulich.dantri.test" $srvs
    makeIISWebFarm "dulich-cdn-test" "cdndulich.dantri.test" $srvs
}

function createduhocTest() {
    AddHostEntry @("duhoc.dantri.test", "cdnduhoc.dantri.test", "duhoc-test", "duhoc-cdn-test")
    $srvs = @(
        @{ip = "127.0.0.1"; port = 51004 }
    )
    makeIISWebFarm "duhoc-test" "duhoc.dantri.test" $srvs
    makeIISWebFarm "duhoc-cdn-test" "cdnduhoc.dantri.test" $srvs
}


function Get-ServerFarm {
    param ([string]$serverFarmName)

    $assembly = [System.Reflection.Assembly]::LoadFrom("$env:systemroot\system32\inetsrv\Microsoft.Web.Administration.dll")
    $mgr = new-object Microsoft.Web.Administration.ServerManager "$env:systemroot\system32\inetsrv\config\applicationhost.config"
    $conf = $mgr.GetApplicationHostConfiguration()
    $section = $conf.GetSection("webFarms")
    $webFarms = $section.GetCollection()
    $webFarm = $webFarms | Where {
        $_.GetAttributeValue("name") -eq $serverFarmName
    }

    $webFarm
}
function PreventNewRequestFarm {
    param ([string]$webFarmName)
    $webFarm = Get-ServerFarm $webFarmName 
    
    $servers = $webFarm.GetCollection()

    $server = $servers | Where {
        $_.GetAttributeValue("address") -eq "192.168.0.192"
    }

    $arr = $server.GetChildElement("applicationRequestRouting")
    $method = $arr.Methods["SetState"]
    $methodInstance = $method.CreateInstance()

    # 0 = Available
    # 1 = Drain
    # 2 = Unavailable
    # 3 = Unavailable Gracefully
    $methodInstance.Input.Attributes[0].Value = 1
    $methodInstance.Execute()
    $counters = $arr.GetChildElement("counters")
    
    # wait for all connection closed
    Do {
        $cont = $counters["currentRequests"] -gt 0
        $cont1 = $counters["requestPerSecond"] -gt 0
        Write-Output "CurrentRequests: $($counters["currentRequests"]) `n RequestPerSecond: $($counters["requestPerSecond"])"
    } while ($cont -and $cont1)
   
}


#$foundRules = Get-WebConfigurationProperty -Name "."  -Filter "/webFarms/webFarm[@name='web-cdn']" | ConvertTo-Json
#echo $foundRules
#AddHostEntry @("m.dantri.com.vn","cdnmobile.dantri.com.vn","mobile1","mobile2")

#createMobileFarm
#PreventNewRequestFarm "mobile" 