(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.filter-slide .filter-slide-range').length > 0 && $('.filter-slide .filter-slide-amount').length > 0) {
            $('.filter-slide-range').each(function () {
                var rangeSlider = this;
                var min = $(rangeSlider).data('min');
                var max = $(rangeSlider).data('max');

                noUiSlider.create(rangeSlider, {
                    connect: true,
                    start: [min, max],
                    step: 1,
                    range: {
                        'min': min,
                        'max': max
                    }
                });

                rangeSlider.noUiSlider.on('update', function (values) {
                    if($(rangeSlider).parent().hasClass('filter-price')) {
                        $(rangeSlider).next().html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + ' triệu</span>' );
                    } else {
                        $(rangeSlider).next().html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + '</span>' );
                    }

                });
            });
        }

        if($('.sidebar-menu-wrap').length > 0) {
            $('.sidebar-menu-wrap').niceScroll({
                horizrailenabled: false
            });
        }

        if($('.filter-list').length > 0) {
            $('.filter-list').niceScroll({
                horizrailenabled: false
            });
        }

        if($('.filter-wrap .filter-title').length > 0) {
            $(document).on('click', '.filter-wrap .filter-title', function () {
                $(this).parent().toggleClass('hidden');
                $(this).next().slideToggle();

                setTimeout(function () {
                    $('.filter-list').getNiceScroll().resize();
                }, 300);
            });
        }
    });

})(jQuery);