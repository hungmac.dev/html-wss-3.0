(function($) {
    'use strict';

    $(document).ready(function() {
        $('.product-img-thumbnail').slick({
            vertical: true,
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            draggable: false
        });

        $(document).on('click', '.product-img-thumbnail .slick-slide', function () {
            var img = $(this).attr('data-item');
            $('.product-img-detail img').attr('src', img);
        });

        $(document).on('click', '.nav-link-wrap .nav-link-item', function () {
            if(!$(this).hasClass('active')) {
                $('.nav-link-wrap .nav-link-item').removeClass('active');
                $(this).addClass('active');

                var tab = $(this).attr('data-tab');
                $('.panel-wrap .panel-content').each(function () {
                    if($(this).attr('data-tab') === tab) {
                        if(!$(this).hasClass('active')) {
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).removeClass('active');
                    }
                });
            }
        });

        $(document).on('click', '.cost-select .cost-input', function () {
            if($(this).next().is(':visible')) {
                $(this).next().slideUp('fast');
            } else {
                $('.cost-select .cost-address-list').slideUp('fast');
                $(this).next().slideDown('fast');
            }
        });

        $(document).on('click', '.cost-address-list .cost-address-item', function () {
            if(!$(this).hasClass('active')) {
                $('.cost-address-list .cost-address-item').removeClass('active');
                $(this).addClass('active');
                $(this).parent().parent().find('.cost-input').html($(this).html());
            }

            $(this).parent().slideUp('fast');
        });

        if($('.cost-address-list').length > 0) {
            $('.cost-address-list').niceScroll({
                horizrailenabled: false
            });
        }

        $(document).on('mouseup', function(e) {
            var container = $('.cost-select');
            if(!container.is(e.target) && container.has(e.target).length === 0 && container.find('.cost-address-list').is(':visible')) {
                container.find('.cost-address-list').slideUp('fast');
            }
        });

        $('.product-box-list').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            draggable: false
        });
    });
})(jQuery);