(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.table-content-list li', function () {
            var offset = 0;
            var content = $(this).attr('data-content');

            $('[data-content]').each(function () {
                if($(this).attr('data-content') === content)  {
                    offset = $(this).offset().top;
                }
            });

            if(offset > 0)
                $('html, body').animate({
                    scrollTop: offset - 16
                }, '300');
        });

        $('.article-related-wrap').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            draggable: false
        });
    });
})(jQuery);