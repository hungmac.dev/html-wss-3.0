(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.search-wrap .search-expand', function () {
            $(this).toggleText('Mở rộng điều kiện tìm kiếm [ + ]', 'Thu gọn điều kiện tìm kiếm [ - ]');
            $('.search-wrap .search-row').toggleClass('expand-active');
            $('.search-wrap .search-col').toggleClass('active');
        });

        $(document).on('click', '.select-wrap .select-item', function () {
            var text = $(this).html();
            var parent = $(this).parent();
            parent.slideToggle('fast');
            parent.prev().html(text);

            if(!$(this).hasClass('active')) {
                parent.find('.select-item').removeClass('active');
                $(this).addClass('active');
            }
        });

        $(document).on('click', '.select-wrap .select-text', function () {
            if(!$(this).next().is(':visible')) {
                $('.select-wrap .select-container').slideUp('fast');
                $(this).next().slideToggle('fast');
            } else {
                $(this).next().slideToggle('fast');
            }
        });

        $(document).on('mouseup', function(e) {
            var container = $('.select-wrap');
            if(!container.is(e.target) && container.has(e.target).length === 0 && container.find('.select-container').is(':visible')) {
                container.find('.select-container').slideUp('fast');
            }
        });

        if($('.select-container').length > 0) {
            $('.select-container').niceScroll({
                horizrailenabled: false
            });
        }
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);