(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.description-more button', function() {
            $(this).toggleText('Mở rộng', 'Rút gọn');
            $('.description-wrap').toggleClass('active');

            $('html, body').animate({
                scrollTop: ($('.description-wrap').offset().top - 30)
            }, 'fast');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);