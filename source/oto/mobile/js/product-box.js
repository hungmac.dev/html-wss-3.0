(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.product-box-tab button', function () {
            var parent = $(this).parent();
            if(!$(this).hasClass('active')) {
                $('.product-box-tab li').removeClass('active');
                parent.addClass('active');

                var tab = $(this).attr('data-tab');
                $('.product-box-panel .product-box-list').each(function () {
                    if($(this).attr('data-tab') === tab) {
                        if(!$(this).hasClass('active'))
                            $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
            }
        });
    });
})(jQuery);