(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.table-content-list a', function () {
            var offset = $($(this).attr('href')).offset().top;

            console.log(offset);

            $('html, body').animate({
                scrollTop: offset - 80
            }, '300');

            return false;
        });
    });
})(jQuery);