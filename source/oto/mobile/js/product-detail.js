(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.nav-link-wrap .nav-link-item', function () {
            if(!$(this).hasClass('active')) {
                $('.nav-link-wrap .nav-link-item').removeClass('active');
                $(this).addClass('active');

                var tab = $(this).attr('data-tab');
                $('.panel-wrap .panel-content').each(function () {
                    if($(this).attr('data-tab') === tab) {
                        if(!$(this).hasClass('active')) {
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).removeClass('active');
                    }
                });
            }
        });
    });
})(jQuery);