(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.search-wrap .search-close, .search-html-form', function () {
            $('.search-wrap').removeClass('active');
            $('body').removeClass('no-scroll');
        });

        var targetInput = $('.search-wrap .search-form-input');
        var fakeInput = $('<input type="text" />').css({
            position: 'absolute',
            width: targetInput.outerWidth(),
            height: 0,
            opacity: 0
        });

        $(document).on('click', '.search-html-form', function () {
            $('.search-wrap').addClass('active');
            $('body').addClass('no-scroll');

            fakeInput.prependTo('.search-wrap').focus();

            setTimeout(function() {
                targetInput.focus();
                fakeInput.remove();
            }, 1000);
        });

        $(window).scroll(function() {
            navigationMenuScroll();
        });

        navigationMenuScroll();
        function navigationMenuScroll() {
            var heightHeader = 0;

            if($('.header-wrap').length > 0)
                heightHeader = $('.header-wrap').outerHeight();

            var elm = $('.search-html');
            var scroll = $(window).scrollTop();

            if(scroll > heightHeader) {
                elm.addClass('fixed');
            } else {
                elm.removeClass('fixed');
            }
        }
    });
})(jQuery);