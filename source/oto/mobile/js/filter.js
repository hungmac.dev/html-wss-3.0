(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.filter-slide .filter-slide-range').length > 0 && $('.filter-slide .filter-slide-amount').length > 0) {
            $('.filter-slide-range').each(function () {
                var rangeSlider = this;
                var min = $(rangeSlider).data('min');
                var max = $(rangeSlider).data('max');

                noUiSlider.create(rangeSlider, {
                    connect: true,
                    start: [min, max],
                    step: 1,
                    range: {
                        'min': min,
                        'max': max
                    }
                });

                rangeSlider.noUiSlider.on('update', function (values) {
                    if($(rangeSlider).parent().hasClass('filter-price')) {
                        $(rangeSlider).next().html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + ' triệu</span>' );
                    } else {
                        $(rangeSlider).next().html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + '</span>' );
                    }
                });
            });
        }

        $(document).on('click', '.filter-clear', function () {
            if($('.filter-slide .filter-slide-range').length > 0 && $('.filter-slide .filter-slide-amount').length > 0) {
                $('.filter-slide-range').each(function () {
                    var rangeSlider = this;
                    rangeSlider.noUiSlider.reset();
                });
            }
        });

        $(document).on('click', '.filter-wrap .filter-close, .sorting-button', function () {
            $('.filter-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
        });

        $(document).on('click', '.filter-choose .filter-input', function () {
            var select = $(this).attr('data-select');
            var value = $(this).attr('data-value');
            var elm = $('.filter-wrap-item[data-select="'+ select +'"]');

            elm.addClass('active').find('li').each(function () {
                if($(this).attr('data-value') === value) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        });

        $(document).on('click', '.filter-wrap-item button', function () {
            if(!$(this).hasClass('active')) {
                $(this).parent().find('button').removeClass('active');
                $(this).addClass('active');
            }

            var parent = $(this).parent().parent();
            var select = parent.attr('data-select');
            var elm = $('.filter-choose .filter-input[data-select="'+ select +'"]');
            elm.attr('data-value', $(this).attr('data-value')).text($(this).text());

            parent.toggleClass('active');
        });

        $(document).on('click touchstart', '.filter-wrap-item .filter-close', function () {
            $(this).parent().parent().toggleClass('active');
        });
    });
})(jQuery);