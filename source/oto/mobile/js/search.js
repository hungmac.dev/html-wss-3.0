(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.search-wrap .search-expand', function () {
            $(this).toggleText('Mở rộng điều kiện tìm kiếm [ + ]', 'Thu gọn điều kiện tìm kiếm [ - ]');
            $('.search-wrap .search-row').toggleClass('expand-active');
        });

        $(document).on('click', '.search-wrap-close', function () {
            $('.search-wrap-container').removeClass('active');
            $('body').removeClass('no-scroll');
        });

        $(document).on('click', '.search-container .select-input', function () {
            $('body').addClass('no-scroll');
            var select = $(this).attr('data-select');
            var value = $(this).attr('data-value');

            $('.search-wrap-container').each(function () {
                if($(this).attr('data-select') === select) {
                    $(this).addClass('active');

                    $(this).find('li').each(function () {
                        if($(this).attr('data-value') === value) {
                            $(this).addClass('active');
                        } else {
                            $(this).removeClass('active');
                        }
                    });
                }
            });
        });

        $(document).on('click', '.search-wrap-list button', function () {
            var value = $(this).attr('data-value');
            var html = $(this).html();
            var parent = $(this).parent().parent();
            var select = parent.attr('data-select');

            if(!$(this).hasClass('active')) {
                parent.find('button').removeClass('active');
                $(this).addClass('active');
            }

            $('.search-wrap .select-input').each(function () {
                if($(this).attr('data-select') === select)
                    $(this).attr('data-value', value).html(html);
            });

            parent.removeClass('active');
            $('body').removeClass('no-scroll');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);