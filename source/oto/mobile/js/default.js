(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.navigation-wrap > li > a, .menu-container > li > a', function (e) {
            var elm = $(this).next();

            if($(this).hasClass('navigation-label')) {
                if($(this).hasClass('has-menu-child')) {
                    if($(this).parent().hasClass('active') && elm.is(':visible')) {
                        $(this).parent().removeClass('active');
                        elm.slideUp('fast');
                    } else {
                        $('.navigation-wrap > li').removeClass('active');
                        $('.navigation-wrap .menu-container').slideUp('fast');
                        $(this).parent().addClass('active');
                        elm.slideDown('fast');
                    }

                    return false;
                }
            } else {
                if(e.offsetX > $(this).width()) {
                    if($(this).parent().hasClass('active') && elm.is(':visible')) {
                        $(this).parent().removeClass('active');
                        elm.slideUp('fast');
                    } else {
                        var parent = $(this).parent().parent();
                        parent.find('li').removeClass('active');
                        parent.find('.submenu').slideUp('fast');
                        $(this).parent().addClass('active');
                        elm.slideDown('fast');
                    }

                    return false;
                }
            }
        });

        $(document).on('click', '.menu-wrap .menu-close, .menu-bar', function () {
            $('.menu-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
        });
    });
})(jQuery);