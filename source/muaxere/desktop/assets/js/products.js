(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.filters-location-label', function () {
            $(this).next().slideToggle('fast');
        });

        $(document).on('click', '.filter-location', function () {
            var parent = $(this).parent(),
                input = parent.prev();

            if(parent.hasClass('active')) {
                parent.slideUp('fast');
            } else {
                parent.find('.filter-location').removeClass('active');
                $(this).addClass('active');
                parent.slideUp('fast');
                input.html($(this).attr('data-value'));
            }
        });

        $(document).on('click', 'body', function (e) {
            if(!$(e.target).hasClass('filters-location-label')) {
                $('.filters-location-list').slideUp('fast');
            }
        });

        new Swiper('.product-swiper', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            lazy: true
        });
    });
})(jQuery);