(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.login-container .login, .login-container .register, .popup-login .overlay, .popup-login .popup-close', function () {
            $('.popup-login').toggleClass('active');
            $('body').toggleClass('scroll-disable');
        });

        $(document).on('click', '.sh-password', function () {
            $(this).toggleText('Hiển thị', 'Ẩn');
            var input = $(this).prev();

            if (input.attr('type') === 'password') {
                input.attr('type', 'text');
            } else {
                input.attr('type', 'password');
            }
        });

        $('#birthday').datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);