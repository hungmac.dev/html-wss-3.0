(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.filters-input', function (e) {
            positionSelect(e);
            var select = $(this).next();

            if(select.is(':hidden'))
                $('.filters-item').slideUp('fast');

            select.slideToggle('fast');
        });

        $(document).on('click', '.filters-option', function () {
            var parent = $(this).parent(),
                input = parent.prev();

            if(parent.hasClass('active')) {
                parent.slideUp('fast');
            } else {
                parent.find('.filters-option').removeClass('active');
                $(this).addClass('active');
                parent.slideUp('fast');
                input.val($(this).attr('data-value'));
            }
        });

        $(document).on('click', '.filters-toggle .filters-label', function () {
            $(this).toggleClass('active').next().slideToggle('fast');
        });

        $(document).on('click', 'body', function (e) {
            if(!$(e.target).hasClass('filters-input')) {
                $('.filters-item').slideUp('fast');
            }
        });

        $(document).on('click', '.filters-selected-item', function () {
            var parent = $(this).parent();
            $(this).remove();
            
            if($.trim(parent.html()) === '')
                parent.parent().parent().remove();
        });

        $(document).on('click', '.filters-clear', function () {
            $(this).parent().parent().remove();
        });
    });

    function positionSelect(e) {
        var elm = e.target,
            position = elm.getBoundingClientRect(),
            height = $(window).height();

        if(height - position.top > 280) {
            $(elm).next().css({'top':'39px', 'bottom':'auto'});
        } else {
            $(elm).next().css({'bottom':'39px', 'top':'auto'});
        }
    }
})(jQuery);