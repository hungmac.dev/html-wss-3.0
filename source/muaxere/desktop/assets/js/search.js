(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.advanced-btn, .advanced-wrap .overlay', function () {
            $('.advanced-wrap').toggleClass('active');
            $('body').toggleClass('scroll-disable');
        });
    });
})(jQuery);