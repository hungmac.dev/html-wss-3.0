(function($) {
    'use strict';

    var swiper;

    $(document).ready(function() {
        productTabActive();

        $(document).on('click', '.nav-products .nav-products-item', function() {
            if(!$(this).hasClass('active')) {
                $('.nav-products .nav-products-item').removeClass('active');
                $(this).addClass('active');
                productTabActive();
            }
        })
    });

    function productTabActive() {
        var panel = $('.nav-products .nav-products-item.active').attr('data-panel');
        $('.products-panel .products-panel-item').hide();
        $('#panel-'+ panel).fadeIn('fast');

        new Swiper('#panel-'+panel+' .products-swiper', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            lazy: true
        });
    }
})(jQuery);