(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.advanced-select-input', function (e) {
            positionSelect(e);
            var select = $(this).next();

            if(select.is(':hidden'))
                $('.advanced-select').slideUp('fast');

            select.slideToggle('fast');
        });

        $(document).on('click', '.advanced-select-item', function () {
            var parent = $(this).parent(),
                input = parent.prev();

            if(parent.hasClass('active')) {
                parent.slideUp('fast');
            } else {
                parent.find('.advanced-select-item').removeClass('active');
                $(this).addClass('active');
                parent.slideUp('fast');
                input.val($(this).attr('data-value'));
            }
        });

        $(document).on('click', '.advanced-wrap', function (e) {
            if(!$(e.target).hasClass('advanced-select-input')) {
                $('.advanced-select').slideUp('fast');
            }
        });

        scrollToTop();
        $(document).on('click', '.back-top', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');
        });

        $(window).scroll(function() {
            scrollToTop();
        });
    });

    function positionSelect(e) {
        var elm = e.target,
            position = elm.getBoundingClientRect(),
            height = $('.advanced-body').outerHeight();

        if(height - position.top > 280) {
            $(elm).next().css({'top':'39px', 'bottom':'auto'});
        } else {
            $(elm).next().css({'bottom':'39px', 'top':'auto'});
        }
    }

    function scrollToTop() {
        var offset = $(window).height();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.back-top').fadeIn('fast');
        } else {
            $('.back-top').fadeOut('fast');
        }
    }
})(jQuery);