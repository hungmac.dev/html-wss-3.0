(function($) {
    'use strict';

    $(document).ready(function() {
        new Swiper('.products-swiper', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            lazy: true
        });
    });
})(jQuery);