(function($) {
    'use strict';

    var navigation;

    $(document).ready(function() {
        navigation = $('.navigation-item.navigation-active');

        $(document).on('click', '.navbar-ico', function () {
            $('.menu-wrap').toggleClass('active');
            $('body').toggleClass('scroll-disable');
        });

        $(document).on('click', '.menu-wrap .button-close, .menu-wrap .overlay', function () {
            $('.menu-wrap').removeClass('active');
            $('body').removeClass('scroll-disable');
        });

        $(document).on('click', '.navigation-search, .search-submit', function () {
            $('.advanced-wrap').toggleClass('active');
            $('body').toggleClass('scroll-disable');

            if($('.navigation-search').hasClass('navigation-active')) {
                $('.navigation-search').removeClass('navigation-active');
                navigation.addClass('navigation-active');
            } else {
                $('.navigation-item').removeClass('navigation-active');
                $('.navigation-search').addClass('navigation-active');
            }
        });

        $(document).on('click', '.advanced-close', function () {
            $('.advanced-wrap').removeClass('active');
            $('body').removeClass('scroll-disable');
            $('.navigation-search').removeClass('navigation-active');
            navigation.addClass('navigation-active');
        });

        $(document).on('click', '.advanced-more', function () {
            $(this).toggleText('Tìm kiếm nâng cao', 'Thu gọn').toggleClass('active');
            $('.advanced-form').toggleClass('active');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);