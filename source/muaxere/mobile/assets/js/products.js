(function($) {
    'use strict';

    $(document).ready(function() {
        new Swiper('.product-swiper', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            lazy: true
        });

        $(document).on('click', '.filters-btn, .products-filter-close', function () {
            $('.products-filter').toggleClass('active');
            $('body').toggleClass('scroll-disable');
        });

        $(document).on('click', '.navigation-search', function () {
            $('.products-filter').removeClass('active');
            $('body').addClass('scroll-disable');
        });

        $(document).on('click', '.filters-selected-item', function () {
            var parent = $(this).parent();
            $(this).remove();

            if(parent.find('.filters-selected-item').length === 0)
                parent.remove();
        });

        $(document).on('click', '.filters-selected-all', function () {
            $(this).parent().remove();
        });
    });
})(jQuery);