// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

// Default
(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.feature-owl').length > 0) {
            $('.feature-owl').owlCarousel({
                autoplay: true,
                loop: true,
                nav: false,
                dots: true,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        if($('.customer-owl').length > 0) {
            var customerOwl = $('.customer-owl');

            customerOwl.on('translate.owl.carousel initialized.owl.carousel', function(e) {
                $(this).find('.owl-nav').removeClass('disabled');
                var elm = $(e.target).find('.owl-item');
                elm.removeClass('owl-left owl-right');
                elm.eq(e.item.index - 1).addClass('owl-left');
                elm.eq(e.item.index + 1).addClass('owl-right');
            }).owlCarousel({
                center: true,
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 3,
                items: 3,
                navText: ['<i class="slider-button slider-left"></i>', '<i class="slider-button slider-right"></i>'],
                mouseDrag: false,
                touchDrag: false
            });

            customerOwl.find('.owl-nav').removeClass('disabled');
        }

        $('#registerSuccess').fancybox();
        $('#registerForm').validate({
            rules: {
                email: {
                    'required' : true,
                    'email' : true,
                    'validateEmail': true
                },
                phone: {
                    'required' : true,
                    'number' : true,
                    'rangelength': [10, 10]
                },
                store: 'required',
                website: 'required',
                address: 'required'
            },
            messages: {
                email: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    email: 'Sai định dạng, vui lòng điền đúng email!',
                    validateEmail: 'Sai định dạng, vui lòng điền đúng email!'
                },
                phone: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    number: 'Sai định dạng, vui lòng điền đúng số điện thoại!',
                    rangelength: 'Sai định dạng, vui lòng điền đúng số điện thoại!'
                },
                store: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                website: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                address: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('#registerSuccess').trigger('click');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('validateEmail', function (value, element) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(value);
        }, '');
    });

    $(document).on('click', '.landing-menu a, .button-register', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 82
        }, 'slow');
    });
})(jQuery);