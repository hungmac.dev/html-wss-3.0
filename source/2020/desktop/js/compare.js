(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', function(e) {
            var container = $('.compare-action-wrap, .compare-product-sticky');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                $('.compare-store-sticky').slideUp();
                $('.compare-region-text, .compare-sort-text, .compare-product-store.hash-same-store').removeClass('active');
                $('.compare-action-child').hide();
            }
        });

        if($('.product-same-price .product-slider').length > 0) {
            $('.product-same-price .product-slider').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 4,
                items: 4,
                margin: 16
            });
        }

        $(document).on('click', '.product-img-thumbnail li', function(e) {
            e.preventDefault();

            if($(this).hasClass('thumb-img')) {
                var imgUrl = $(this).data('image');
                $('.product-img-detail img').attr('src', imgUrl);
            } else if($(this).hasClass('thumb-video')) {
                $('#video').slideToggle();
            }
        });

        $(document).on('click', '.read-more span', function(e) {
            e.preventDefault();
            $(this).toggleClass('active').toggleText('Xem thêm', 'Rút gọn').parent().parent().toggleClass('active');

            if(!$(this).hasClass('active')) {
                var offset = $(this).offset().top;
                var height = $(window).height();
                $('html, body').animate({
                    scrollTop: offset - height/2
                }, 'slow');
            }
        });

        $(document).on('click', '.compare-same-store', function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                $('.compare-same-store, .compare-shipping .compare-shipping-label, .compare-gift .compare-gift-label').removeClass('active');
                $('.compare-gift .compare-gift-detail').hide();
                $('.compare-shipping .compare-shipping-detail, .product-same-store').slideUp()
            }
            $(this).toggleClass('active');

            var storeId = $(this).data('store-id');
            $('#product-store-' + storeId).slideToggle();
        });

        $(document).on('click', '.compare-shipping .compare-shipping-label', function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                $('.compare-same-store, .compare-shipping .compare-shipping-label, .compare-gift .compare-gift-label').removeClass('active');
                $('.compare-gift .compare-gift-detail').hide();
                $('.compare-shipping .compare-shipping-detail, .product-same-store').slideUp()
            }
            $(this).toggleClass('active').next().slideToggle();
        });

        $(document).on('click', '.compare-gift .compare-gift-label', function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                $('.compare-same-store, .compare-shipping .compare-shipping-label, .compare-gift .compare-gift-label').removeClass('active');
                $('.compare-gift .compare-gift-detail').hide();
                $('.compare-shipping .compare-shipping-detail, .product-same-store').slideUp()
            }
            $(this).toggleClass('active').next().toggle();
        });

        $(document).on('click', '.compare-region-parent .compare-region-text, .compare-sort-inner .compare-sort-text', function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                $('.compare-region-parent .compare-region-text, .compare-sort-inner .compare-sort-text').removeClass('active');
                $('.compare-action-child').hide();
            }
            $(this).toggleClass('active').next().toggle();
        });

        $(document).on('click', '.compare-product-store.hash-same-store', function(e) {
            e.preventDefault();
            $(this).toggleClass('active').next().slideToggle();

            if($('.compare-store-sticky ul').length > 0 && $('.compare-store-sticky ul').is(':visible')) {
                $('.compare-store-sticky ul').getNiceScroll().resize();
            }
        });

        $(document).on('click', '.compare-sticky-list .scroll-div', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $($(this).data('target')).offset().top - 148
            }, 'slow');
        });

        if($('.compare-store-sticky ul').length > 0) {
            $('.compare-store-sticky ul').niceScroll({
                horizrailenabled: false
            });
        }

        if($('.compare-region-child').length > 0) {
            $('.compare-region-child').niceScroll({
                horizrailenabled: false
            });
        }

        menuCompareSticky();
    });

    $(window).scroll(function() {
        menuCompareSticky();
    });

    function menuCompareSticky() {
        var offset = $('.navigation-wrap').outerHeight() + 35;
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.compare-sticky').show();
        } else {
            $('.compare-sticky').hide();
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });

    $(document).on('click', '.compare-item-action .compare-chat', function(e) {
        e.preventDefault();

        $('.facebook-messenger iframe').attr('src', $(this).attr('href'));
        $('.facebook-messenger').addClass('active')
    });

    $(document).on('click', '.facebook-messenger-close img', function(e) {
        e.preventDefault();
        $('.facebook-messenger').removeClass('active')
    });
})(jQuery);