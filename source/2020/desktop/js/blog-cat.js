(function($) {
    'use strict';

    $(document).ready(function () {
        menuBlogScroll();
    });

    $(window).scroll(function() {
        menuBlogScroll();
    });

    function menuBlogScroll() {
        var offset = $('.top-bar').outerHeight() + $('.header-wrap').outerHeight() - $('.navigation-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.menu-blog-wrap').addClass('active');
        } else {
            $('.menu-blog-wrap').removeClass('active');
        }
    }
})(jQuery);