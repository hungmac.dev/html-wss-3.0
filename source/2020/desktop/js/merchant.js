(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.read-more span', function() {
            $(this).toggleText('Xem thêm', 'Rút gọn');
            $('.category-description-wrap').toggleClass('active');
        });

        if($('.price-range').length > 0 && $('.price-amount').length > 0) {
            $('.price-range').slider({
                range: true,
                min: 0,
                max: 200,
                values: [ 0, 200 ],
                slide: function( event, ui ) {
                    $('.price-amount').html( '<span>' + ui.values[0] + '</span><span>' + ui.values[1] + ' triệu</span>' );
                }
            });

            $('.price-amount').html( '<span>' + $('.price-range').slider('values', 0) + '</span><span>' + $('.price-range').slider('values', 1) + ' triệu</span>' );
        }

        if($('.filter-list ol, .merchant-address ul').length > 0) {
            $('.filter-list ol, .merchant-address ul').niceScroll({
                horizrailenabled: false
            });
        }

        $(document).on('click', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle();
        });

        $(document).on('click', '.toc-wrap a', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 'slow');
        });

        $(document).on('click', '.filter-box-item .filter-box-title', function(e) {
            e.preventDefault();
            $(this).toggleClass('active').next().slideToggle();

            setTimeout(function () {
                $('.filter-list ol').getNiceScroll().resize();
            }, 500);
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);