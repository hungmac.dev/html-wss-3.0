(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.deal-product-title li a').length > 0 && $('.deal-product-content .deal-product-panel').length > 0) {
            $(document).on('click', '.deal-product-title li a', function(e) {
                e.preventDefault();
                $('.deal-product-title li a').removeClass('active');
                $(this).addClass('active');
                $('.deal-product-content .deal-product-panel').hide();
                $($(this).attr('href')).show();
            });
        }

        menuBlogScroll();
    });

    $(window).scroll(function() {
        menuBlogScroll();
    });

    function menuBlogScroll() {
        var offset = $('.top-bar').outerHeight() + $('.header-wrap').outerHeight() - $('.navigation-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.menu-blog-wrap').addClass('active');
        } else {
            $('.menu-blog-wrap').removeClass('active');
        }
    }
})(jQuery);