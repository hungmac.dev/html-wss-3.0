(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle();
        });

        $(document).on('click', '.toc-wrap a', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 135
            }, 'slow');
        });

        if($('.article-related-slider').length > 0) {
            $('.article-related-slider').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: false,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 3,
                items: 3,
                margin: 24
            });
        }

        menuBlogScroll();
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });

    $(window).scroll(function() {
        menuBlogScroll();
    });

    function menuBlogScroll() {
        var offset = $('.top-bar').outerHeight() + $('.header-wrap').outerHeight() - $('.navigation-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.menu-blog-wrap').addClass('active');
        } else {
            $('.menu-blog-wrap').removeClass('active');
        }
    }
})(jQuery);