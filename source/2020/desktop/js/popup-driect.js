(function($) {
    'use strict';

    $(document).ready(function () {
        popupDriectCount();

        $(document).on('click', '.popup-driect-close', function() {
            $('.popup-driect-wrap').removeClass('active');
        });

        function popupDriectCount() {
            var myTimer = document.getElementById('driectTimer');
            var t = 5;
            var intervalCounter = 0;

            window.timer = window.setInterval(function () {
                intervalCounter ++;
                myTimer.innerHTML = (t - intervalCounter) + 's';

                if (intervalCounter >= 5) {
                    window.clearInterval(window.timer);
                    $('.popup-driect-wrap').removeClass('active');
                }
            }.bind(this), 1000);
        }
    });
})(jQuery);