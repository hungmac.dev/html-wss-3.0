(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('touchstart', '.read-more span', function(e) {
            $(this).toggleClass('active').toggleText('Xem thêm', 'Rút gọn').parent().parent().toggleClass('active');

            if(!$(this).hasClass('active')) {
                var offset = $(this).offset().top;
                var height = $(window).height();
                $('html, body').animate({
                    scrollTop: offset - height/2
                }, 'slow');
            }
        });

        $(document).on('touchstart', '.compare-item-action .compare-same-store', function(e) {
            $('.compare-same-store').removeClass('active');

            var storeId = $(this).data('store-id');
            if($('#product-store-' + storeId).is(":visible")) {
                $('#product-store-' + storeId).slideToggle();
            } else {
                $(this).addClass('active');
                $('.product-same-store').slideUp();
                $('#product-store-' + storeId).slideToggle();
            }
        });

        $(document).on('touchstart', '.compare-item-action .compare-gift', function(e) {
            var giftId = $(this).data('gift-id');
            $('#product-gift-' + giftId).toggleClass('active');
        });

        $(document).on('touchstart', '.product-gift-close', function(e) {
            $(this).parent().parent().toggleClass('active');
        });

        $(document).on('touchstart', '.gallery-product .thumbnail-video, .product-video .video-close', function () {
            $('#video').toggleClass('active');
        });

        $(document).on('touchstart', '.filter-parent .filter-list-item', function() {
            $('.filter-parent .sub-filter').hide();
            $(this).find('.sub-filter').show();
        });

        $(document).on('touchstart', '.compare-sticky-list .scroll-div', function(e) {
            $('html, body').animate({
                scrollTop: $($(this).data('target')).offset().top - 100
            }, 'slow');
        });

        $(document).on('touchstart', '.compare-product-sticky .hash-same-store', function(e) {
            $(this).toggleClass('active');
            $('.compare-store-sticky').slideToggle();
        });

        menuMobileSticky();

        $(document).on('touchstart', function(e) {
            var container = $('.product-compare-sticky');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                $('.compare-store-sticky').slideUp();
                $('.compare-product-sticky .hash-same-store').removeClass('active');
            }
        });

        $(window).scroll(function() {
            menuMobileSticky();
        });
    });

    function menuMobileSticky() {
        $('.search-header').removeClass('relative');
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();
        var height = $('.product-compare-sticky').outerHeight();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
            $('.menu-compare-sticky, .product-compare-sticky').addClass('active');
            if(height > 45) {
                $('.wrong-price-icon').removeClass('active').css('bottom', height + 'px');
            } else {
                $('.wrong-price-icon').removeAttr('style').addClass('active');
            }
        } else {
            $('.search-header').removeClass('fixed');
            $('.menu-compare-sticky, .product-compare-sticky').removeClass('active');
            $('.wrong-price-icon').removeAttr('style').removeClass('active');
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);