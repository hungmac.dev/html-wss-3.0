(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('touchstart', '.read-more span', function() {
            $(this).toggleText('Xem thêm', 'Rút gọn');
            $('.category-description-wrap').toggleClass('active');
        });

        $(document).on('touchstart', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle();
        });

        $(document).on('touchstart', '.toc-wrap a', function(e) {
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 'slow');
        });

        $(document).on('click', '.gift-icon', function(e) {
            e.preventDefault();
            $(this).addClass('active');
            var giftId = $(this).data('gift-id');
            $('#product-gift-' + giftId).addClass('active');
        });

        $(document).on('click', '.product-gift-close', function(e) {
            $(this).parent().parent().removeClass('active');
            $('.gift-icon.active').removeClass('active');
        });

        menuMobileSticky();

        $(window).scroll(function() {
            menuMobileSticky();
        });
    });

    function menuMobileSticky() {
        $('.search-header').removeClass('relative');
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);