(function($) {
    'use strict';

    $(document).ready(function() {
        $('.banner-wrap').slick({
            infinite: false,
            dots: true
        });
    });
})(jQuery);