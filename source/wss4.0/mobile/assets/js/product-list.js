(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.product-list-gift', function(e) {
            $(this).addClass('active');
            var giftId = $(this).data('gift-id');
            $('#product-gift-' + giftId).addClass('active');

            return false;
        });

        $(document).on('click', '.product-gift-close', function(e) {
            $(this).parent().removeClass('active');
            $('.product-list-gift').removeClass('active');
        });
    });
})(jQuery);