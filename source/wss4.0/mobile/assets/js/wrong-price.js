(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.wrong-price-button, .wrong-price .wrong-price-close', function(e) {
            $('.wrong-price').toggleClass('active');
            $('body').toggleClass('no-scroll');
        });

        $(document).on('click', '.wrong-success-back span', function(e) {
            $('.wrong-price-wrap').removeClass('success');
            $('.wrong-price-success').removeClass('active');
        });

        $('#wrongPrice').validate({
            rules: {
                'reason': {
                    'wrongPrice': true
                },
                'comment': {
                    'wrongPrice': true
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {

                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('wrongPrice', function (value, element) {
            var check = false;
            var reason = $('input[name="reason"]:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            if(reason.length !== 0) {
                $('.wrong-price-reason .wrong-error-text').html('').hide();
                $('.wrong-price-checkbox label').removeClass('wrong-error');

                if($.inArray('5', reason) >= 0) {
                    var comment = $('#comment').val();
                    if(comment !== '') {
                        $('.wrong-price-comment .wrong-error-text').html('').hide();
                        $('.wrong-price-comment textarea').removeClass('wrong-error');
                        check = true;
                    } else {
                        $('.wrong-price-comment textarea').addClass('wrong-error');
                        $('.wrong-price-comment .wrong-error-text').html('Nội dung bắt buộc, vui lòng điền thông tin').show();
                    }
                } else {
                    $('.wrong-price-comment .wrong-error-text').html('').hide();
                    $('.wrong-price-comment textarea').removeClass('wrong-error');
                    check = true;
                }
            } else {
                $('.wrong-price-reason .wrong-error-text').html('Nội dung bắt buộc, vui lòng chọn tối thiểu 1 vấn đề!').show();
                $('.wrong-price-checkbox label').addClass('wrong-error');
            }

            return check;
        }, '');
    });
})(jQuery);