(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.navigation-submenu-more button', function(e) {
            $(this).toggleText('Mở rộng', 'Thu lại').toggleClass('active').parent().prev().toggleClass('active');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);