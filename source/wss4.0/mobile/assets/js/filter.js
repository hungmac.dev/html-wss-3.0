(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.filter-slide .filter-slide-range').length > 0 && $('.filter-slide .filter-slide-amount').length > 0) {
            $('.filter-slide-range').each(function () {
                var rangeSlider = this;
                var min = $(rangeSlider).data('min');
                var max = $(rangeSlider).data('max');

                noUiSlider.create(rangeSlider, {
                    connect: true,
                    start: [min, max],
                    step: 1,
                    range: {
                        'min': min,
                        'max': max
                    }
                });

                rangeSlider.noUiSlider.on('update', function (values) {
                    $(rangeSlider).next().html( '<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + ' triệu</span>' );
                });
            });
        }

        $(document).on('click', '.filter-clear', function () {
            if($('.filter-slide .filter-slide-range').length > 0 && $('.filter-slide .filter-slide-amount').length > 0) {
                $('.filter-slide-range').each(function () {
                    var rangeSlider = this;
                    rangeSlider.noUiSlider.reset();
                });
            }
        });

        $(document).on('click', '.filter-wrap .filter-close, .filter-ico', function () {
            $('.filter-wrap').toggleClass('active');
            $('body').toggleClass('no-scroll');
        });

        $(document).on('click touchstart', '.filter-choose .filter-input', function () {
            var select = $(this).attr('data-select');
            var value = $(this).attr('data-value');
            var elm = $('.filter-wrap-item[data-select="'+ select +'"]');

            elm.addClass('active').find('li').each(function () {
                if($(this).attr('data-value') === value) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        });

        $(document).on('touchstart', '.filter-wrap-item li, .filter-wrap-item .filter-arrow', function (e) {
            if($(this).hasClass('filter-arrow')) {
                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).next().slideUp('fast');
                } else {
                    $('.filter-list .filter-arrow').removeClass('active');
                    $(this).addClass('active');
                    $('.filter-list .filter-children').slideUp('fast');
                    $(this).next().slideDown('fast');
                }

                return false;
            } else {
                var text, select, elm, parent;
                parent = $(this).parent();

                if(parent.hasClass('filter-children')) {
                    if(!$(this).hasClass('active')) {
                        $(this).parent().parent().parent().find('li').removeClass('active');
                        $(this).addClass('active');
                    }

                    select = parent.parent().parent().parent().attr('data-select');
                    text = parent.parent().html();
                } else {
                    if(!$(this).hasClass('active')) {
                        $(this).parent().find('li').removeClass('active');
                        $(this).addClass('active');
                    }

                    select = parent.parent().attr('data-select');
                    text = $(this).html();
                }

                elm = $('.filter-choose .filter-input[data-select="'+ select +'"]');
                elm.attr('data-value', $(this).attr('data-value')).text(removeTagsHTML(text));
                $('.filter-wrap-item[data-select="'+ select +'"]').removeClass('active');

                return false;
            }
        });

        $(document).on('click', '.filter-wrap-item .filter-close', function () {
            $(this).parent().parent().toggleClass('active');
        });

        function removeTagsHTML(str) {
            if((str === null) || (str === ''))
                return false;
            else
                str = str.toString();

            return str.replace(/(\r\n|\n|\r)/gm, '').replace(/<(\w+)[^>]*>.*<\/\1>/gi, '');
        }
    });
})(jQuery);