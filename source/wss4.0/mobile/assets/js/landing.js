(function($) {
    'use strict';

    $(document).ready(function () {
        $('.feature-slider').slick({
            infinite: false,
            dots: true,
            arrows: false
        });

        $('.customer-testimonial').slick({
            infinite: false,
            dots: false
        });

        $('#registerSuccess').fancybox();
        $('#registerForm').validate({
            rules: {
                email: {
                    'required' : true,
                    'email' : true,
                    'validateEmail': true
                },
                phone: {
                    'required' : true,
                    'number' : true,
                    'rangelength': [10, 10]
                },
                store: 'required',
                website: 'required',
                address: 'required'
            },
            messages: {
                email: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    email: 'Sai định dạng, vui lòng điền đúng email!',
                    validateEmail: 'Sai định dạng, vui lòng điền đúng email!'
                },
                phone: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    number: 'Sai định dạng, vui lòng điền đúng số điện thoại!',
                    rangelength: 'Sai định dạng, vui lòng điền đúng số điện thoại!'
                },
                store: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                website: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                address: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('#registerSuccess').trigger('click');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('validateEmail', function (value, element) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(value);
        }, '');

        $(document).on('click', '.button-register', function() {
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 60
            }, 'slow');
        });
    });
})(jQuery);