(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.landing-coupon-btn', function () {
            var $temp = $('<input>');
            $('body').append($temp);
            $temp.val($('.landing-coupon-code').text()).select();
            document.execCommand('copy');
            $temp.remove();
        });

        $(document).on('click', '.landing-product-btn', function () {
            var id = $(this).attr('data-combo');
            changeCombo(id);
            $('.popup-product-select').val(id);
            $('.popup-product-coupon').val($('.landing-coupon-code').text())
            $('.popup-product-wrap').fadeIn('fast');
        });

        $(document).on('click', '.popup-product-overlay, .popup-product-close', function () {
            $('.popup-product-wrap').fadeOut('fast');
        });

        $('.popup-product-select').change(function () {
            var id = $(this).val();
            changeCombo(id);
        });
    });

    function changeCombo(id) {
        var combo = {
            1: {
                "size":"16L",
                "sale":"1.399.000 VND",
                "regular":"1.799.000 VND"
            },
            2: {
                "size":"22L",
                "sale":"1.499.000 VND",
                "regular":"1.899.000 VND"
            },
            3: {
                "size":"32L",
                "sale":"1.599.000 VND ",
                "regular":"1.999.000 VND"
            },
        };

        $('.popup-product-icon').html(combo[id]['size']);
        $('.popup-product-sale').html(combo[id]['sale']);
        $('.popup-product-regular').html(combo[id]['regular']);
    }
})(jQuery);