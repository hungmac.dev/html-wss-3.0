WebFontConfig = {
    google: {
        families: [ 'Roboto:ital,wght@0,400;0,500;0,700;0,900;1,400;1,500;1,700;1,900&display=swap' ]
    },
    timeout: 500
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = false;
    wf.defer = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click touchstart', function(e) {
            if($('.navigation-wrap').hasClass('active')) {
                var container = $('.navigation-wrap, .menu-bar');

                if(!container.is(e.target) && container.has(e.target).length === 0) {
                    $('.navigation-wrap').removeClass('active');
                    $('body').removeClass('no-scroll');
                }
            }
        });

        $(document).on('click', '.search-wrap .search-text, .search-sidebar .search-sidebar-close', function(e) {
            $('.search-sidebar').toggleClass('active');
            $('body').toggleClass('no-scroll');
        });

        $(document).on('click', '.menu-bar', function(e) {
            $('.navigation-wrap').addClass('active');
            $('body').addClass('no-scroll');
        });

        $(document).on('click', '.navigation-wrap .navigation-title, .navigation-wrap .sub-menu .has-children a', function(e) {
            var elm = $(this).next();

            if($(this).hasClass('navigation-title')) {
                if($(this).parent().hasClass('has-children')) {
                    $('.navigation-wrap .sub-menu .has-children a').removeClass('active');
                    $('.navigation-wrap .sub-menu .sub-menu').slideUp('fast');

                    if($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        elm.slideUp('fast');
                    } else {
                        $('.navigation-wrap .navigation-title').removeClass('active');
                        $(this).addClass('active');
                        $('.navigation-wrap .sub-menu').slideUp('fast');
                        elm.slideDown('fast');
                    }

                    return false;
                }
            } else {
                if(e.offsetX > $(this).width()) {
                    if($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        elm.slideUp('fast');
                    } else {
                        $('.navigation-wrap .sub-menu .has-children a').removeClass('active');
                        $(this).addClass('active');
                        $('.navigation-wrap .sub-menu .sub-menu').slideUp('fast');
                        elm.slideDown('fast');
                    }

                    return false;
                }
            }
        });

        $(window).scroll(function() {
            navigationMenuScroll();
        });

        navigationMenuScroll();
        function navigationMenuScroll() {
            var heightHeader = 0;
            var heightSlogan = 0;

            if($('.header-wrap').length > 0)
                heightHeader = $('.header-wrap').outerHeight();

            if($('.slogan-wrap').length > 0)
                heightSlogan = $('.slogan-wrap').outerHeight();

            var elm = $('.search-wrap');
            var scroll = $(window).scrollTop();

            if(scroll > (heightHeader + heightSlogan)) {
                elm.addClass('fixed');
            } else {
                elm.removeClass('fixed');
            }
        }
    });
})(jQuery);