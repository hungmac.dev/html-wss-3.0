(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.faqs-popup-open', function () {
            $('.popup-faqs').addClass('active');
            $('body').addClass('scroll-disable');
        });

        $(document).on('click', '.popup-faqs-close', function () {
            $('.popup-faqs').removeClass('active');
            $('body').removeClass('scroll-disable');
        });

        $('.popup-faqs-file').change(function (e) {
            var elm = $(this).parent().parent().find('.uploads-wrap');
            readURLImg(this, elm);
        });

        $(document).on('click', '.popup-faqs-uploads', function () {
            $(this).parent().find('.popup-faqs-file').trigger('click');
        });

        $(document).on('click', '.upload-remove', function () {
            $(this).parent().remove();
        });

        $(document).on('click', '.nav-submenu-more', function() {
            $(this).toggleText('Mở rộng', 'Thu lại').toggleClass('active').prev().toggleClass('active');
        });
    });

    function readURLImg(input, elm) {
        if(input.files && input.files[0]) {
            var item = $('<span class="upload-item"><span class="upload-img"></span><button type="button" class="upload-remove"></button></span>'),
                img = document.createElement('img'),
                reader = new FileReader();

            reader.onload = function(e) {
                img.src = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
            item.find('.upload-img').append(img);
            $(elm).append(item);
        }
    }

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);