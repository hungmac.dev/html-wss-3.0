(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.product-compare-gift', function(e) {
            var giftId = $(this).data('gift-id');
            $('#product-gift-' + giftId).addClass('active');
        });

        $(document).on('click', '.product-gift-close', function(e) {
            $(this).parent().removeClass('active');
        });

        $(document).on('click', '.product-compare-more', function(e) {
            var compareId = $(this).data('compare');

            if(!$(this).hasClass('active')) {
                $('.product-compare .product-compare-more').removeClass('active');
                $('.product-compare .product-compare').slideUp('fast');
            }

            $(this).toggleClass('active');
            $('#product-compare-' + compareId).slideToggle('fast');
        });

        $(document).on('click', '.show-more', function(e) {
            $(this).toggleText('Xem thêm', 'Rút gọn');
            var elm = $('#' + $(this).data('show'));
            elm.toggleClass('active');

            if(!elm.hasClass('active')) {
                var offset = elm.offset().top;
                $('html, body').animate({
                    scrollTop: offset - 108
                }, 'fast');
            }
        });

        $(document).on('click', '.navigation-compare button', function(e) {
            var scrollId = $(this).data('scroll');

            $('html, body').animate({
                scrollTop: $('#compare-scroll-' + scrollId).offset().top - 108
            }, 'fast');
        });

        $(document).on('touchstart', '.compare-sticky-buy', function(e) {
            $(this).toggleClass('active');
            $('.compare-sticky-store').slideToggle('fast');
        });

        $(window).scroll(function() {
            stickyWrap();
        });

        stickyWrap();

        function stickyWrap() {
            var offset = $('.header-wrap').outerHeight();
            var scroll = $(window).scrollTop();

            if(scroll > offset) {
                $('.navigation-compare, .compare-sticky').addClass('active');

                var height = $('.compare-sticky').outerHeight() <= 62 ? $('.compare-sticky').outerHeight() : 62;

                if(!$('.compare-sticky-store').is(':visible'))
                    $('.wrong-price-button').css('bottom', height + 'px');
            } else {
                $('.compare-sticky-store').slideUp('fast');
                $('.navigation-compare, .compare-sticky').removeClass('active');
                $('.wrong-price-button').removeAttr('style');
            }
        }
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);