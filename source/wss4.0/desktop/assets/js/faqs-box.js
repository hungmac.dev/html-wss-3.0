(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.faqs-form-uploads', function () {
            $(this).parent().find('.faqs-form-file').trigger('click');
        });

        $('.faqs-form-file').change(function (e) {
            var elm = $(this).parent().parent().find('.faqs-uploads');
            readURLImg(this, elm);
        });

        $(document).on('click', '.upload-remove', function () {
            $(this).parent().remove();
        });
    });

    function readURLImg(input, elm) {
        if(input.files && input.files[0]) {
            var item = $('<span class="upload-item"><span class="upload-img"></span><button type="button" class="upload-remove"></button></span>'),
                img = document.createElement('img'),
                reader = new FileReader();

            reader.onload = function(e) {
                img.src = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
            item.find('.upload-img').append(img);
            $(elm).append(item);
        }
    }
})(jQuery);