(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', function(e) {
            var container = $('.navigation-product, .compare-action-wrap');

            if(!container.is(e.target) && container.has(e.target).length === 0) {
                $('.compare-store-wrap, .compare-action-child').slideUp('fast');
                $('.navigation-product, .compare-action-parent .compare-action-text').removeClass('active');
            }
        });

        $(document).on('click', '.navigation-product .compare-store.has-store', function() {
            $(this).parent().toggleClass('active');
            $(this).next().slideToggle('fast');
        });

        $(document).on('click', '.product-thumb-item', function() {
            if(!$(this).hasClass('product-thumb-video')) {
                var imgUrl = $(this).attr('data-image');
                $('.product-thumb-show img').attr('src', imgUrl);
            } else {
                $('#video').slideToggle('fast');
            }
        });

        $(document).on('click', '.compare-action-parent .compare-action-text', function() {
            if(!$(this).hasClass('active')) {
                $('.compare-action-parent .compare-action-text').removeClass('active');
                $('.compare-action-child').hide();
            }

            $(this).toggleClass('active').next().slideToggle('fast');
        });

        $(document).on('click', '.read-more span', function() {
            $(this).toggleClass('active').toggleText('Xem thêm', 'Rút gọn');
            $('#' + $(this).attr('data-elm')).toggleClass('active');

            if(!$(this).hasClass('active')) {
                var offset = $(this).offset().top;
                var height = $(window).height();
                $('html, body').animate({
                    scrollTop: offset - height/2
                }, 'fast');
            }
        });

        $(document).on('click', '.compare-shipping-label', function() {
            if(!$(this).hasClass('active')) {
                $('.compare-shipping-label, .compare-gift-label').removeClass('active');
                $('.compare-gift').hide();
                $('.compare-shipping').slideUp('fast');
            }
            $(this).toggleClass('active').next().slideToggle();
        });

        $(document).on('click', '.compare-gift-label', function() {
            if(!$(this).hasClass('active')) {
                $('.compare-shipping-label, .compare-gift-label').removeClass('active');
                $('.compare-gift').hide();
                $('.compare-shipping').slideUp('fast');
            }
            $(this).toggleClass('active').next().toggle();
        });

        $(document).on('click', '.compare-same-button', function() {
            $('.compare-shipping-label, .compare-gift-label').removeClass('active');
            $('.compare-gift').hide();
            $('.compare-shipping').slideUp('fast');

            if(!$(this).hasClass('active')) {
                $('.compare-same-store').slideUp('fast');
            }
            $(this).toggleClass('active');

            var storeId = $(this).data('store');
            $('#product-store-' + storeId).slideToggle('fast');
        });

        $(document).on('click', '.compare-action-chat', function(e) {
            e.preventDefault();

            $('.facebook-messenger iframe').attr('src', $(this).attr('href'));
            $('.facebook-messenger').addClass('active')
        });

        $(document).on('click', '.facebook-messenger-close', function() {
            $('.facebook-messenger').removeClass('active')
        });

        if($('.compare-product-list').length > 0) {
            $('.compare-product-list').niceScroll({
                horizrailenabled: false
            });
        }

        if($('.compare-action-child').length > 0) {
            $('.compare-action-child').niceScroll({
                horizrailenabled: false
            });
        }

        $('.product-same-slider').slick({
            slidesToShow: 4,
            infinite: false
        });

        $(window).scroll(function() {
            navigationSticky();
        });

        navigationSticky();

        function navigationSticky() {
            var offset = $('.navigation-wrap').outerHeight() + 35;
            var scroll = $(window).scrollTop();

            if(scroll > offset) {
                $('.navigation-sticky').addClass('active');
            } else {
                $('.navigation-sticky').removeClass('active');
            }
        }

        $.fn.extend({
            toggleText: function(a, b){
                return this.text(this.text() == b ? a : b);
            }
        });

        $(document).on('click', '.faqs-form-uploads', function () {
            $(this).parent().find('.faqs-form-file').trigger('click');
        });

        $('.faqs-form-file').change(function (e) {
            var elm = $(this).parent().parent().find('.faqs-uploads');
            readURLImg(this, elm);
        });

        $(document).on('click', '.upload-remove', function () {
            $(this).parent().remove();
        });
    });

    function readURLImg(input, elm) {
        if(input.files && input.files[0]) {
            var item = $('<span class="upload-item"><span class="upload-img"></span><button type="button" class="upload-remove"></button></span>'),
                img = document.createElement('img'),
                reader = new FileReader();

            reader.onload = function(e) {
                img.src = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
            item.find('.upload-img').append(img);
            $(elm).append(item);
        }
    }
})(jQuery);