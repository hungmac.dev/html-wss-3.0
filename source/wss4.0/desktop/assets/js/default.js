WebFontConfig = {
    google: {
        families: [ 'Roboto:ital,wght@0,400;0,500;0,700;0,900;1,400;1,500;1,700;1,900&display=swap' ]
    },
    timeout: 500
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = false;
    wf.defer = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function() {
         $('.brand-wrap').slick({
             slidesToShow: 8,
             infinite: false
         });

        $(document).on('click', function(e) {
            var container = $('.login-wrap');

            if(!container.is(e.target) && container.has(e.target).length === 0 && container.is(':visible')) {
                $('.login-container').hide();
            }
        });
         
         $(document).on('click', '.login-title', function () {
             $('.login-container').toggle();
         });

        scrollToTop();

        $(document).on('click', '.scroll-top', function() {
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');

            return false;
        });

        $(window).scroll(function() {
            scrollToTop();
        });
    });

    function scrollToTop() {
        var offset = $(window).height();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    }
})(jQuery);