(function($) {
    'use strict';

    $(document).ready(function() {
        $('.article-related-wrap').slick({
            slidesToShow: 3,
            infinite: false
        });
    });
})(jQuery);