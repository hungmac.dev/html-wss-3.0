(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.toc-wrap span', function() {
            $(this).toggleText('[xem]', '[ẩn]');
            $('.toc-wrap ul').slideToggle('fast');
        });

        $(document).on('click', '.toc-wrap a', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 152
            }, 'fast');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);