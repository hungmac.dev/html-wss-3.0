(function($) {
    'use strict';

    $(document).ready(function() {
        if($('.merchant-info .merchant-address').length > 0) {
            $('.merchant-info .merchant-address').niceScroll({
                horizrailenabled: false
            });
        }
    });
})(jQuery);