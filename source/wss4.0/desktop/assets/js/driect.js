WebFontConfig = {
    google: {
        families: [ 'Roboto:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap' ]
    },
    timeout: 500
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = false;
    wf.defer = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function() {
        var myTimer = document.getElementById('timer');
        var t = 5;
        var intervalCounter = 0;

        window.timer = window.setInterval(function () {
            intervalCounter ++;
            myTimer.innerHTML = (t - intervalCounter) + 's';

            if (intervalCounter >= 5) {
                window.clearInterval(window.timer);
            }
        }.bind(this), 1000);
    });
})(jQuery);