(function($) {
    'use strict';

    $(document).ready(function() {
        if($('.sidebar-menu .sidebar-menu-wrap').length > 0) {
            $('.sidebar-menu .sidebar-menu-wrap').niceScroll({
                horizrailenabled: false
            });
        }
    });
})(jQuery);