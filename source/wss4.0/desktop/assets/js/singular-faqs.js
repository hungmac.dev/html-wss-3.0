(function($) {
    'use strict';

    $(document).ready(function() {
        $('.topic-highlight-box .topic-highlight-title').each(function () {
            var parent = $(this).parent(),
                child = parent.find('.topic-highlight-content');
            if(parent.hasClass('active'))
                child.slideDown();
        });

        $(document).on('click', '.topic-highlight-box .topic-highlight-title', function () {
            var parent = $(this).parent(),
                child = parent.find('.topic-highlight-content');
            parent.toggleClass('active');
            child.slideToggle('fast');
        });

        $(document).on('click', '.faqs-popup-open', function () {
            $('.popup-faqs').addClass('active');
            $('body').addClass('scroll-disable');
        });

        $(document).on('click', '.popup-faqs-close, .popup-faqs .popup-overlay', function () {
            $('.popup-faqs').removeClass('active');
            $('body').removeClass('scroll-disable');
        });

        $(document).on('click', '.popup-faqs-uploads', function () {
            $(this).parent().find('.popup-faqs-file').trigger('click');
        });

        $('.popup-faqs-file').change(function (e) {
            var elm = $(this).parent().parent().parent().find('.uploads-wrap');
            readURLImg(this, elm);
        });

        $(document).on('click', '.faqs-reply-uploads', function () {
            $(this).parent().find('.faqs-reply-file').trigger('click');
        });

        $('.faqs-reply-file').change(function (e) {
            var elm = $(this).parent().parent().find('.uploads-wrap');
            readURLImg(this, elm);
        });

        $(document).on('click', '.upload-remove', function () {
            $(this).parent().remove();
        });

        $(document).on('click', '.popup-message-close, .popup-message-wrap .popup-overlay', function () {
            $('.popup-message').removeClass('active');
            $('body').removeClass('scroll-disable');
        });
    });

    function readURLImg(input, elm) {
        if(input.files && input.files[0]) {
            var item = $('<span class="upload-item"><span class="upload-img"></span><button type="button" class="upload-remove"></button></span>'),
                img = document.createElement('img'),
                reader = new FileReader();

            reader.onload = function(e) {
                img.src = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
            item.find('.upload-img').append(img);
            $(elm).append(item);
        }
    }
})(jQuery);