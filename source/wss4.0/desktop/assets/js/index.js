(function($) {
    'use strict';

    $(document).ready(function() {
        $('.banner-primary').slick({
            infinite: false,
            dots: true
        });

        $('.navigation-list li.has-children').hover(function() {
            $('.navigation-list li.has-children .sub-menu').removeAttr('style');

            var position = $(this).position();
            var top = position.top;
            var hCurrent = $(this).outerHeight();
            var hSubmenu = $(this).find('.sub-menu').outerHeight();
            var htWindow = $(window).height();

            if(hSubmenu > htWindow - top) {
                $(this).find('.sub-menu').css('top', (top + hCurrent - hSubmenu) + 'px');
            } else {
                $(this).find('.sub-menu').css('top', top + 'px');
            }
        });

        $('.navigation-list').niceScroll({
            horizrailenabled: false
        });
    });
})(jQuery);