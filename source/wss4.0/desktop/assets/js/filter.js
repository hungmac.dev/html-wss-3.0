(function($) {
    'use strict';

    $(document).ready(function() {
        var rangeSlider = document.getElementById('slideRange');
        var min = $(rangeSlider).data('min');
        var max = $(rangeSlider).data('max');

        noUiSlider.create(rangeSlider, {
            connect: true,
            start: [min, max],
            step: 1,
            range: {
                'min': min,
                'max': max
            }
        });

        rangeSlider.noUiSlider.on('update', function (values) {
            $(rangeSlider).next().html('<span>' + Number(values[0]) + '</span><span>' + Number(values[1]) + ' triệu</span>');
        });

        if($('.filter-list').length > 0) {
            $('.filter-list').niceScroll({
                horizrailenabled: false
            });
        }

        if($('.filter-wrap .filter-title').length > 0) {
            $(document).on('click', '.filter-wrap .filter-title', function () {
                $(this).parent().toggleClass('hidden');
                $(this).next().slideToggle('fast');

                setTimeout(function () {
                    $('.filter-list').getNiceScroll().resize();
                }, 300);
            });
        }
    });
})(jQuery);