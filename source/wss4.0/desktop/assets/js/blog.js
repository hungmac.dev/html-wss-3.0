(function($) {
    'use strict';

    $(document).ready(function() {
        fixedMenuBlog();

        $(window).scroll(function() {
            fixedMenuBlog();
        });
    });

    function fixedMenuBlog() {
        var scroll = $(window).scrollTop();

        if(scroll > 80) {
            $('.navigation-blog').addClass('active');
        } else {
            $('.navigation-blog').removeClass('active');
        }
    }
})(jQuery);