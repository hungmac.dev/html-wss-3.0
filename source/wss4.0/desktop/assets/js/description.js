(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.desc-more', function() {
            $(this).toggleText('Xem thêm', 'Rút gọn');

            if($('.desc-content').hasClass('active')) {
                $('html, body').animate({
                    scrollTop: $('.desc-content').offset().top - 135
                }, 'fast');
            }

            $('.desc-content').toggleClass('active');
        });
    });

    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
})(jQuery);