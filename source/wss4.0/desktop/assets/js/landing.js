WebFontConfig = {
    google: {
        families: [ 'Roboto:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap' ]
    },
    timeout: 500
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = false;
    wf.defer = true;
    s.parentNode.insertBefore(wf, s);
})(document);

(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.landing-menu li, .button-register', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $($(this).attr('data-scroll')).offset().top - 82
            }, 'fast');
        });

        $('.feature-slick').slick({
            arrows: false,
            infinite: false,
            dots: true
        });

        $('.customer-slick').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '0',
            touchMove: false
        });

        $(document).on('click', '.customer-slick .slick-arrow', function(e) {
            slick_add_class();
        });

        slick_add_class();
        function slick_add_class() {
            $('.customer-slick .slick-slide').removeClass('slick-left slick-right');
            var elm = $('.slick-current');
            elm.prev().addClass('slick-left');
            elm.next().addClass('slick-right');
        }

        $('#registerSuccess').fancybox();
        $('#registerForm').validate({
            rules: {
                email: {
                    'required' : true,
                    'email' : true,
                    'validateEmail': true
                },
                phone: {
                    'required' : true,
                    'number' : true,
                    'rangelength': [10, 10]
                },
                store: 'required',
                website: 'required',
                address: 'required'
            },
            messages: {
                email: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    email: 'Sai định dạng, vui lòng điền đúng email!',
                    validateEmail: 'Sai định dạng, vui lòng điền đúng email!'
                },
                phone: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!',
                    number: 'Sai định dạng, vui lòng điền đúng số điện thoại!',
                    rangelength: 'Sai định dạng, vui lòng điền đúng số điện thoại!'
                },
                store: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                website: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                },
                address: {
                    required: 'Nội dung bắt buộc, vui lòng điền thông tin!'
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(response) {
                        $('#registerSuccess').trigger('click');
                    },
                    error: function (errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        });

        $.validator.addMethod('validateEmail', function (value, element) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(value);
        }, '');
    });
})(jQuery);