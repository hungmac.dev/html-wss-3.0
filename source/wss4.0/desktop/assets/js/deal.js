(function($) {
    'use strict';

    $(document).ready(function() {
        $(document).on('click', '.deal-menu h3', function() {
            if(!$(this).hasClass('active')) {
                $('.deal-menu h3').removeClass('active');
                $(this).addClass('active');

                var deal = $(this).attr('data-deal');
                $('.deal-product').removeClass('active');
                $('#deal_' + deal).addClass('active');
            }
        });
    });
})(jQuery);