(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.action-post', function () {
            var elm = $(this).next();

            if($(elm).is(':visible')) {
                elm.slideUp('fast');
            } else {
                $('.action-list').slideUp('fast');
                elm.slideDown('fast');
            }
        });

        $(document).on('click', 'body', function (e) {
            if(!$(e.target).hasClass('action-post'))
                $('.action-list').slideUp('fast');
        });

        $(document).on('click', '.cpanel-head', function () {
            $(this).toggleClass('cpanel-hide');
            $(this).next().slideToggle('fast');
        });

        $(document).on('click', '.cnav-item', function () {
            if(!$(this).hasClass('active')) {
                $('.cnav-item').removeClass('active');
                $(this).addClass('active');

                var id = $(this).data('id');
                $('.cpanel-item').hide();
                $('#cpanel_' + id).fadeIn('fast');
            }
        });

        $(document).on('click', '.inav-item', function () {
            if(!$(this).hasClass('active')) {
                $('.inav-item').removeClass('active');
                $(this).addClass('active');

                var id = $(this).data('id');
                $('.ipanel-item').hide();
                $('#ipanel_' + id).fadeIn('fast');
            }
        });

        $(document).on('click', '.ipnav-item', function () {
            if(!$(this).hasClass('active')) {
                $(this).parent().find('.ipnav-item').removeClass('active');
                $(this).addClass('active');

                var id = $(this).data('id'),
                    elm = $('#ipwrap_' + id);
                elm.parent().find('.ipanel-container').hide();
                elm.fadeIn('fast');
            }
        });

        $(document).on('click', '.ipsame-view, .ipslist-view', function () {
            $(this).toggleClass('active');
            $(this).parent().next().slideToggle('fast');
        });

        $(document).on('click', '.ilreport-view, .create-incident', function () {
            $($(this).data('src')).toggleClass('active');
            $('body').toggleClass('body-fixed');
        });

        stickNavFooter();
        $(window).resize(function () {
            stickNavFooter();
        });
    });

    function stickNavFooter() {
        var hwindow = $(window).height(),
            helm = $('.logo').outerHeight() + $('.menu-wrap').outerHeight() + 230;

        if(hwindow - helm < 0) {
            $('.nav-wrap').addClass('nav-relative');
        } else {
            $('.nav-wrap').removeClass('nav-relative');
        }
    }
})(jQuery);