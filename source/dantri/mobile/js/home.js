(function($) {
    'use strict';

    $(document).ready(function () {
        if($('.banner-carousel').length > 0) {
            $('.banner-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                dots: true,
                lazyLoad: true,
                autoplayHoverPause: true,
                lazyLoadEager: 1,
                items: 1
            });
        }

        menuMobileSticky();
    });

    $(window).scroll(function() {
        menuMobileSticky();
    });

    function menuMobileSticky() {
        var offset = $('.header-wrap').outerHeight();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.search-header').addClass('fixed');
        } else {
            $('.search-header').removeClass('fixed');
        }
    }
})(jQuery);