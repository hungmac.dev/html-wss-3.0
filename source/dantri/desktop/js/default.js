// Web Font Loader
WebFontConfig = {
    google: {
        families: [ 'Roboto:400,400i,500,500i,700,700i&display=swap' ]
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

// Default
(function($) {
    'use strict';

    $(document).ready(function () {
        scrollToTop();
    });

    $(document).on('click', '.scroll-top', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');

        return false;
    });

    $(window).scroll(function() {
        scrollToTop();
    });

    function scrollToTop() {
        var offset = $(window).height();
        var scroll = $(window).scrollTop();

        if(scroll > offset) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    }

})(jQuery);