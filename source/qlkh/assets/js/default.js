(function($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click', '.action-wrap .action-ico', function () {
            var next = $(this).next();
            if(next.hasClass('active')) {
                next.removeClass('active');
            } else {
                $('.action-wrap .navbar-action').removeClass('active');
                next.addClass('active');
            }
        });

        if($('.info-table-wrap').length > 0) {
            $('.info-table-wrap').niceScroll({
                cursorcolor: '#C4C4C4'
            });
        }

        if($('.filter-list, .status-list').length > 0) {
            $('.filter-list, .status-list').niceScroll({
                cursorcolor: '#333',
                horizrailenabled: false
            });
        }

        $(document).on('click', '.info-status', function (e) {
            var elm = $(this).find('.status-list');
            if($(e.target).prop('class') !== 'status-item' && elm.is(':visible')) {
                elm.slideUp('fast');
            } else {
                if($(e.target).prop('class') === 'status-item') {
                    elm.find('.status-item').removeClass('active');
                    $(e.target).addClass('active');
                    $(this).find('.status-text').html($(e.target).html());
                    $(this).removeClass('status-success status-warning status-danger').addClass($(e.target).attr('data-class'));
                    elm.slideUp('fast');
                } else {
                    $('.status-list').slideUp('fast');
                    elm.slideDown('fast');
                }
            }
        });

        $(document).on('click', '.tab-control button', function () {
            if(!$(this).hasClass('active')) {
                $('.tab-control button').removeClass('active');
                $(this).addClass('active');

                var tab = $(this).attr('data-tab');
                $('.tab-action button').each(function () {
                    if($(this).attr('data-tab') == tab) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });

                $('.tab-container .tab-content').removeClass('active');
                $('#' + tab).addClass('active');
            }

            return false;
        });

        if($('.form-control-select').length > 0) {
            $('.form-control-select').select2();

            $('#domain').on('select2:open', function () {
                $('.domain .select2-results').append('<button class="btn-ajax-load">Xem thêm...</button>');
            }).on('select2:closing', function () {
                $('.btn-ajax-load').remove();
            });
        }

        $(document).on('click', '.upload-remove', function () {
             $(this).parent().remove();
        });

        $(document).on('click', '.filter-select', function (e) {
            var elm = $(this).find('.filter-list');
            if($(e.target).prop('class') !== 'filter-item' && elm.is(':visible')) {
                elm.slideUp('fast');
            } else {
                if($(e.target).prop('class') === 'filter-item') {
                    elm.find('.filter-item').removeClass('active');
                    $(e.target).addClass('active');
                    $(this).find('.filter-text').html($(e.target).html());
                    elm.slideUp('fast');
                } else {
                    $('.filter-list').slideUp('fast');
                    elm.slideDown('fast');
                }
            }
        });

        $(document).on('mouseup touchstart', function(e) {
            var container = $('.filter-select');

            if(!container.is(e.target) && container.has(e.target).length === 0 && $('.filter-list').is(':visible')) {
                $('.filter-list').slideUp('fast');
            }
        });

        $(document).on('click', '.btn-collapse', function () {
            $(this).toggleClass('active');
            $('.body-row').toggleClass('sidebar-collapse');
        });

        $('.sidebar .nav-link').hover(function () {
            var title = $(this).attr('title');
            $(this).data('tooltipText', title).removeAttr('title');
            $('<div class="wss-tooltip"></div>').text(title).appendTo('.body-row');
        }, function () {
            $(this).attr('title', $(this).data('tooltipText'));
            $('.wss-tooltip').remove();
        }).mousemove(function() {
            var offset = 6,
                cursorX = $(this).outerWidth(),
                cursorY = $(this).offset().top,
                tooltipHeight = $('.wss-tooltip').height();

            $('.wss-tooltip').css({
                '-webkit-transform': 'translate(' + (cursorX + offset) + 'px, ' + (cursorY + tooltipHeight/2) + 'px)',
                '-ms-transform': 'translate(' + (cursorX + offset) + 'px, ' + (cursorY + tooltipHeight/2) + 'px)',
                'transform': 'translate(' + (cursorX + offset) + 'px, ' + (cursorY + tooltipHeight/2) + 'px)'
            });
        });

        $('.tab-control button, .tab-action-wrap .tab-action-toggle').hover(function () {
            var title = $(this).attr('title');
            $(this).data('tooltipText', title).removeAttr('title');
            $('<div class="wss-tooltip wss-tooltip-active wss-tooltip-top"></div>').text(title).appendTo('body');
        }, function () {
            $(this).attr('title', $(this).data('tooltipText'));
            $('.wss-tooltip').remove();
        }).mousemove(function() {
            var offset = 35,
                cursorX = $(this).offset().top,
                cursorY = $(this).offset().left,
                tooltipWidth = $('.wss-tooltip').width();

            $('.wss-tooltip').css({
                '-webkit-transform': 'translate(' + (cursorY - tooltipWidth/2) + 'px, ' + (cursorX + offset) + 'px)',
                '-ms-transform': 'translate(' + (cursorY - tooltipWidth/2) + 'px, ' + (cursorX + offset) + 'px)',
                'transform': 'translate(' + (cursorY - tooltipWidth/2) + 'px, ' + (cursorX + offset) + 'px)'
            });
        });

        $(document).on('click', '.tab-action-toggle', function () {
            $('.navbar-btn-wrap').toggleClass('active');
            $('.main-row').toggleClass('main-collapse');
            setTimeout(function () {
                if($('.info-table-wrap').length > 0)
                    $('.info-table-wrap').getNiceScroll().resize();
            }, 300);
        });

        if($('.form-group-contract .table-line').length > 0) {
            $('.form-group-contract .table-line').niceScroll({
                cursorcolor: '#C4C4C4'
            });
        }

        $(document).on('click', '.contract-service', function () {
            var value = $(this).attr('.data-service');
            $(this).toggleClass('active');

            if($(this).hasClass('active')) {
                $('.table-line .table .service-row').each(function () {
                    if($(this).attr('.data-service') === value) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
            } else {
                $('.table-line .table .service-row').each(function () {
                    if($(this).attr('.data-service') === value)
                        $(this).removeClass('active');
                });
            }
        });

        $(document).on('click', '.user-wrap', function () {
            $('.user-action').slideToggle('fast');
        });

        $(document).on('click', '.notify-wrap', function () {
            $('.notify-list').slideToggle('fast');
        });

        if($('.notify-list').length > 0) {
            $('.notify-list').niceScroll({
                cursorcolor: '#C4C4C4'
            });
        }
    });
})(jQuery);