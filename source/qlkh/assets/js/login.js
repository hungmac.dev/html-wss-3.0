(function($) {
    'use strict';

    $(document).ready(function () {
        $('#login').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: 'required'
            },
            messages: {
                email: '*Địa chỉ email không hợp lệ',
                password: '*Mật khẩu không đúng'
            },
            submitHandler: function(form) {

            }
        });
    });
})(jQuery);